<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public function cek() {
        // get logged-in user
        $user = auth()->user();
        $user->givePermissionTo('Melihat daftar pegawai');

        // get all inherited permissions for that user
        $permissions = $user->getAllPermissions();

        return response()->json([
            'b' => $permissions
        ], 200);
    }
}
