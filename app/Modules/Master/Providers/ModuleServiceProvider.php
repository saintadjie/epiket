<?php

namespace App\Modules\Master\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('master', 'Resources/Lang', 'app'), 'master');
        $this->loadViewsFrom(module_path('master', 'Resources/Views', 'app'), 'master');
        $this->loadMigrationsFrom(module_path('master', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('master', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('master', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
