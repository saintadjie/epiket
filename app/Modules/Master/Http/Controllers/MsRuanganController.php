<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MsRuangan;
use App\Models\MsPusdakim;

use App\User;

use DB;
use Hash;
use Auth;
use DataTables;

class MsRuanganController extends Controller
{
    public function index(Request $request) {
		$userLevel          = Auth::user()->level_pengguna;

        if($userLevel == '1') {

		    $rs     = MsRuangan::select('ms_ruangan.id', 'kode_ruangan', 'nama_ruangan', 'ms_ruangan.keterangan', 'nama_pusdakim', 'ms_ruangan.deleted_at')
                        ->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
		                ->get();
		    return view('master::msruangan.index', ['rs' => $rs]);

	    } else {

            $rs     = MsRuangan::select('ms_ruangan.id', 'kode_ruangan', 'nama_ruangan', 'ms_ruangan.keterangan', 'nama_pusdakim', 'ms_ruangan.deleted_at')
                        ->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
                        ->where('deleted_at','=', NULL)
                        ->get();

            return view('master::msruangan.index', ['rs' => $rs]);

        }

	}

	public function page_add() {

        $rs     	= MsRuangan::select('id', 'kode_ruangan', 'nama_ruangan', 'keterangan', 'kode_pusdakim')
	                    		->get();

	    $rspusdakim = MsPusdakim::select('kode_pusdakim', 'nama_pusdakim', DB::raw("CONCAT(kode_pusdakim, ' - ', nama_pusdakim) as display"))
					  			->get();

        return view('master::msruangan.add', ['rs' => $rs, 'rspusdakim' => $rspusdakim]);

    }

    public function store(Request $request) {

        $check      = MsRuangan::where('kode_ruangan', $request->kode_ruangan)->first();
        $check2     = MsRuangan::onlyTrashed()->where('kode_ruangan', $request->kode_ruangan)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        $rs 				= new MsRuangan();
        $rs->kode_pusdakim	= $request->kode_pusdakim;
        $rs->kode_ruangan	= $request->kode_ruangan;
        $rs->nama_ruangan 	= $request->nama_ruangan;
        $rs->keterangan 	= $request->keterangan;
        $rs->save();

        return response()->json(['status' => 'OK']);

    }

    public function page_show($id) {

		$rs 		= MsRuangan::findOrfail($id);
		
		$rspusdakim = MsPusdakim::select('kode_pusdakim', 'nama_pusdakim', DB::raw("CONCAT(kode_pusdakim, ' - ', nama_pusdakim) as display"))
					  			->get();

        return view('master::msruangan.show', ['rs' => $rs, 'rspusdakim' => $rspusdakim]);

	}

	public function edit(Request $request) {

		$kode_ruangan           = $request->kode_ruangan;
        $kode_ruanganlama  		= $request->kode_ruanganlama;


		if ($kode_ruanganlama != $kode_ruangan) {
        	$check 		= MsRuangan::where('kode_ruangan', $request->kode_ruangan)->first();
        	$check2 	= MsRuangan::onlyTrashed()->where('kode_ruangan', $request->kode_ruangan)->first();

        	if (!empty($check)) {
        		return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        	}
        	elseif (!empty($check2)) {
				return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
			}
        }

		$rs 				= MsRuangan::find($request->id);
		$rs->kode_pusdakim	= $request->kode_pusdakim;
		$rs->kode_ruangan 	= $request->kode_ruangan;
		$rs->nama_ruangan 	= $request->nama_ruangan;
		$rs->keterangan 	= $request->keterangan;
		$rs->save();

		return response()->json(['status' => 'OK']);
	}

	public function delete($id) {

        $rs = MsRuangan::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

    	$rs     = MsRuangan::select('ms_ruangan.id', 'kode_ruangan', 'nama_ruangan', 'ms_ruangan.keterangan', 'nama_pusdakim', 'ms_ruangan.deleted_at')
                	->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
              		->onlyTrashed()
		        	->get();


    	return view('master::msruangan.trash', ['rs' => $rs]);

	}

	public function restore($id)
	{
	    	$rs = MsRuangan::onlyTrashed()->where('id',$id);
	    	$rs->restore();

	    	return response()->json(['status' => 'OK']);
	}


	public function deletepermanent($id)
	{
	    	$rs = MsRuangan::onlyTrashed()->where('id',$id);
	    	$rs->forceDelete();

	    	return response()->json(['status' => 'OK']);
	}
}
