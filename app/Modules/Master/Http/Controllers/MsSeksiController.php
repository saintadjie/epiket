<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MsSeksi;
use App\Models\MsSubdirektorat;

use App\User;

use DB;
use Hash;
use Auth;
use DataTables;

class MsSeksiController extends Controller
{
    public function index(Request $request) {
		$userLevel          = Auth::user()->level_pengguna;

        if($userLevel == '1') {

		    $rs     = MsSeksi::select('ms_seksi.id', 'kode_seksi', 'nama_seksi', 'nama_subdirektorat', 'ms_seksi.deleted_at')
                        ->join('ms_subdirektorat', 'ms_seksi.kode_subdirektorat', 'ms_subdirektorat.kode_subdirektorat')
		                ->get();
		    return view('master::msseksi.index', ['rs' => $rs]);

	    } else {

            $rs     = MsSeksi::select('ms_seksi.id', 'kode_seksi', 'nama_seksi', 'nama_subdirektorat', 'ms_seksi.deleted_at')
                        ->join('ms_subdirektorat', 'ms_seksi.kode_subdirektorat', 'ms_subdirektorat.kode_subdirektorat')
                        ->where('deleted_at','=', NULL)
                        ->get();

            return view('master::msseksi.index', ['rs' => $rs]);

        }

	}

	public function page_add() {

        $rs     			= MsSeksi::select('id', 'kode_seksi', 'nama_seksi', 'kode_subdirektorat')
	                    		->get();

	    $rssubdirektorat 	= MsSubdirektorat::select('kode_subdirektorat', 'nama_subdirektorat', DB::raw("CONCAT(kode_subdirektorat, ' - ', nama_subdirektorat) as display"))
					  			->get();

        return view('master::msseksi.add', ['rs' => $rs, 'rssubdirektorat' => $rssubdirektorat]);

    }

    public function store(Request $request) {

        $check      = MsSeksi::where('kode_seksi', $request->kode_seksi)->first();
        $check2     = MsSeksi::onlyTrashed()->where('kode_seksi', $request->kode_seksi)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        $rs                       	= new MsSeksi();
        $rs->kode_subdirektorat     = $request->kode_subdirektorat;
        $rs->kode_seksi				= $request->kode_seksi;
        $rs->nama_seksi 			= $request->nama_seksi;
        $rs->save();

        return response()->json(['status' => 'OK']);

    }

    public function page_show($id) {

		$rs 		= MsSeksi::findOrfail($id);
		
		$rssub 		= MsSubdirektorat::select('kode_subdirektorat', 'nama_subdirektorat', DB::raw("CONCAT(kode_subdirektorat, ' - ', nama_subdirektorat) as display"))
					  			->get();

        return view('master::msseksi.show', ['rs' => $rs, 'rssub' => $rssub]);

	}

	public function edit(Request $request) {

		$kode_seksi            = $request->kode_seksi;
        $kode_seksilama  		= $request->kode_seksilama;


		if ($kode_seksilama != $kode_seksi) {
        	$check 		= MsSeksi::where('kode_seksi', $request->kode_seksi)->first();
        	$check2 	= MsSeksi::onlyTrashed()->where('kode_seksi', $request->kode_seksi)->first();

        	if (!empty($check)) {
        		return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        	}
        	elseif (!empty($check2)) {
				return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
			}
        }

		$rs 						= MsSeksi::find($request->id);
        $rs->kode_subdirektorat     = $request->kode_subdirektorat;
		$rs->kode_seksi 			= $request->kode_seksi;
		$rs->nama_seksi 			= $request->nama_seksi;
		$rs->save();

		return response()->json(['status' => 'OK']);
	}

	public function delete($id) {

        $rs = MsSeksi::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

    	$rs     = MsSeksi::select('ms_seksi.id', 'kode_seksi', 'nama_seksi', 'nama_subdirektorat', 'ms_seksi.deleted_at')
                        ->join('ms_subdirektorat', 'ms_seksi.kode_subdirektorat', 'ms_subdirektorat.kode_subdirektorat')
                        ->onlyTrashed()
		                ->get();


    	return view('master::msseksi.trash', ['rs' => $rs]);

	}

	public function restore($id)
	{
	    	$rs = MsSeksi::onlyTrashed()->where('id',$id);
	    	$rs->restore();

	    	return response()->json(['status' => 'OK']);
	}


	public function deletepermanent($id)
	{
	    	$rs = MsSeksi::onlyTrashed()->where('id',$id);
	    	$rs->forceDelete();

	    	return response()->json(['status' => 'OK']);
	}
}
