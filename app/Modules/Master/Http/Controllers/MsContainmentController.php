<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MsContainment;
use App\Models\MsRuangan;
use App\Models\MsPusdakim;

use App\User;

use DB;
use Hash;
use Auth;
use DataTables;


class MsContainmentController extends Controller
{
    public function index(Request $request) {
		$userLevel          = Auth::user()->level_pengguna;

        if($userLevel == '1') {

		    $rs     = MsContainment::select('ms_containment.id', 'kode_containment', 'nama_containment', 'ms_containment.keterangan', 'nama_ruangan', 'nama_pusdakim', 'ms_containment.deleted_at')
		    			->join('ms_ruangan', 'ms_containment.kode_ruangan', 'ms_ruangan.kode_ruangan')
                        ->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
		                ->get();
		    return view('master::mscontainment.index', ['rs' => $rs]);

	    } else {

            $rs     = MsContainment::select('ms_containment.id', 'kode_containment', 'nama_containment', 'ms_containment.keterangan', 'nama_ruangan', 'nama_pusdakim', 'ms_containment.deleted_at')
		    			->join('ms_ruangan', 'ms_containment.kode_ruangan', 'ms_ruangan.kode_ruangan')
                        ->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
                        ->where('deleted_at','=', NULL)
                        ->get();

            return view('master::mscontainment.index', ['rs' => $rs]);

        }

	}

	public function page_add() {

		$rs     	= MsContainment::select('id', 'kode_containment', 'nama_containment', 'keterangan', 'kode_ruangan')
		                ->get();

        $rsruangan 	= MsRuangan::select('kode_ruangan', 'nama_ruangan', DB::raw("CONCAT(kode_ruangan, ' - ', nama_ruangan) as display"))
	                    ->get();

	    $rspusdakim = MsPusdakim::select('kode_pusdakim', 'nama_pusdakim', DB::raw("CONCAT(kode_pusdakim, ' - ', nama_pusdakim) as display"))
					  	->get();

        return view('master::mscontainment.add', ['rs' => $rs, 'rsruangan' => $rsruangan, 'rspusdakim' => $rspusdakim]);

    }

    public function pullData (Request $request) {
        $type       = $request->type;

        switch ($type) {
            case 'pullData':
                if ($request->has('kode_pusdakim')) {
                    $kode_pusdakim  = $request->kode_pusdakim;
                    $rs             = DB::table('ms_ruangan')
                                        ->select('kode_ruangan', 'nama_ruangan', DB::raw('CONCAT(nama_ruangan) as display'))
                                        ->where('kode_pusdakim', $kode_pusdakim)
                                        ->get();
                    return response()->json($rs);

                } else if ($request->has('kode_containment')) {
                    $kode_containment  = $request->kode_containment;
                    $rs              = DB::table('ms_containment')
                                        ->select('ms_containment.kode_containment', 'ms_containment.nama_containment', 'ms_ruangan.kode_ruangan', 'ms_ruangan.nama_ruangan', 'ms_pusdakim.kode_pusdakim', 'ms_pusdakim.nama_pusdakim')
                                        ->join('ms_ruangan', 'ms_containment.kode_ruangan', 'ms_ruangan.kode_ruangan')
                                        ->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
                                        ->where('ms_containment.kode_containment', $kode_containment)
                                        ->first();
                    return response()->json($rs);

                }

            break;
        }
    }

    public function store(Request $request) {

        $check      = MsContainment::where('kode_containment', $request->kode_containment)->first();
        $check2     = MsContainment::onlyTrashed()->where('kode_containment', $request->kode_containment)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        $rs 					= new MsContainment();
        $rs->kode_ruangan		= $request->kode_ruangan;
        $rs->kode_containment	= $request->kode_containment;
        $rs->nama_containment 	= $request->nama_containment;
        $rs->keterangan 		= $request->keterangan;
        $rs->save();

        return response()->json(['status' => 'OK']);

    }

    public function page_show($id) {

        $rs         = MsContainment::findOrfail($id);

        $rsruangan  = MsRuangan::select('kode_ruangan', 'nama_ruangan', DB::raw("CONCAT(kode_ruangan, ' - ', nama_ruangan) as display"))
                        ->get();

        $rspusdakim = MsPusdakim::select('kode_pusdakim', 'nama_pusdakim', DB::raw("CONCAT(kode_pusdakim, ' - ', nama_pusdakim) as display"))
                        ->get();
        return view('master::mscontainment.show', ['rs' => $rs, 'rsruangan' => $rsruangan, 'rspusdakim' => $rspusdakim]);

    }

    public function edit(Request $request) {

        $kode_containment           = $request->kode_containment;
        $kode_containmentlama       = $request->kode_containmentlama;


        if ($kode_containmentlama != $kode_containment) {
            $check      = MsContainment::where('kode_containment', $request->kode_containment)->first();
            $check2     = MsContainment::onlyTrashed()->where('kode_containment', $request->kode_containment)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
            elseif (!empty($check2)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
            }
        }

        $rs                     = MsContainment::find($request->id);
        $rs->kode_ruangan       = $request->kode_ruangan;
        $rs->kode_containment   = $request->kode_containment;
        $rs->nama_containment   = $request->nama_containment;
        $rs->keterangan         = $request->keterangan;
        $rs->save();

        return response()->json(['status' => 'OK']);
    }

    public function delete($id) {

        $rs = MsContainment::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

        $rs     = MsContainment::select('ms_containment.id', 'kode_containment', 'nama_containment', 'ms_containment.keterangan', 'nama_ruangan', 'nama_pusdakim', 'ms_containment.deleted_at')
                        ->join('ms_ruangan', 'ms_containment.kode_ruangan', 'ms_ruangan.kode_ruangan')
                        ->join('ms_pusdakim', 'ms_ruangan.kode_pusdakim', 'ms_pusdakim.kode_pusdakim')
                        ->onlyTrashed()
                        ->get();

        return view('master::mscontainment.trash', ['rs' => $rs]);

    }

    public function restore($id)
    {
            $rs = MsContainment::onlyTrashed()->where('id',$id);
            $rs->restore();

            return response()->json(['status' => 'OK']);
    }


    public function deletepermanent($id)
    {
            $rs = MsContainment::onlyTrashed()->where('id',$id);
            $rs->forceDelete();

            return response()->json(['status' => 'OK']);
    }
}
