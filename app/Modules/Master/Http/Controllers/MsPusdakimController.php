<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MsPusdakim;

use App\User;

use DB;
use Hash;
use Auth;
use DataTables;

class MsPusdakimController extends Controller
{
    public function index(Request $request) {
		$userLevel          = Auth::user()->level_pengguna;

        if($userLevel == '1') {

		    $rs     = MsPusdakim::select('id', 'kode_pusdakim', 'nama_pusdakim', 'lokasi_pusdakim', 'keterangan', 'deleted_at')
		                ->get();
		    return view('master::mspusdakim.index', ['rs' => $rs]);

	    } else {

            $rs     = Mspusdakim::select('id', 'kode_pusdakim', 'nama_pusdakim', 'lokasi_pusdakim', 'keterangan', 'deleted_at')
                        ->where('deleted_at','=', NULL)
                        ->get();

            return view('master::mspusdakim.index', ['rs' => $rs]);

        }

	}

	public function page_add() {

        $rs     = MsPusdakim::select('id', 'kode_pusdakim', 'nama_pusdakim', 'lokasi_pusdakim', 'keterangan')
	                    ->get();

        return view('master::mspusdakim.add', ['rs' => $rs]);

    }

    public function store(Request $request) {

        $check      = MsPusdakim::where('kode_pusdakim', $request->kode_pusdakim)->first();
        $check2     = MsPusdakim::onlyTrashed()->where('kode_pusdakim', $request->kode_pusdakim)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        $rs                 	= new MsPusdakim();
        $rs->kode_pusdakim		= $request->kode_pusdakim;
        $rs->nama_pusdakim 		= $request->nama_pusdakim;
        $rs->lokasi_pusdakim 	= $request->lokasi_pusdakim;
        $rs->keterangan 		= $request->keterangan;
        $rs->save();

        return response()->json(['status' => 'OK']);

    }

    public function page_show($id) {

		$rs 	= MsPusdakim::findOrfail($id);
		
		return view('master::mspusdakim.show', ['rs' => $rs]);

	}


	public function edit(Request $request) {

		$kode_pusdakim 		= $request->kode_pusdakim;
        $kode_pusdakimlama 	= $request->kode_pusdakimlama;


		if ($kode_pusdakimlama != $kode_pusdakim) {
        	$check 		= MsPusdakim::where('kode_pusdakim', $request->kode_pusdakim)->first();
        	$check2 	= MsPusdakim::onlyTrashed()->where('kode_pusdakim', $request->kode_pusdakim)->first();

        	if (!empty($check)) {
        		return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        	}
        	elseif (!empty($check2)) {
				return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
			}
        }

		$rs 					= MsPusdakim::find($request->id);
		$rs->kode_pusdakim 		= $request->kode_pusdakim;
		$rs->nama_pusdakim 		= $request->nama_pusdakim;
		$rs->lokasi_pusdakim 	= $request->lokasi_pusdakim;
        $rs->keterangan 		= $request->keterangan;
		$rs->save();

		return response()->json(['status' => 'OK']);
	}

	public function delete($id) {

        $rs = MsPusdakim::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

    	$rs = MsPusdakim::onlyTrashed()->get();

    	return view('master::mspusdakim.trash', ['rs' => $rs]);

	}

	public function restore($id)
	{
	    	$rs = MsPusdakim::onlyTrashed()->where('id',$id);
	    	$rs->restore();

	    	return response()->json(['status' => 'OK']);
	}


	public function deletepermanent($id)
	{
	    	$rs = MsPusdakim::onlyTrashed()->where('id',$id);
	    	$rs->forceDelete();

	    	return response()->json(['status' => 'OK']);
	}
}
