<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MsJenisKegiatan;

use App\User;

use DB;
use Auth;
use DataTables;

class MsJenisKegiatanController extends Controller
{
    public function index(Request $request) {
		$userLevel          = Auth::user()->level_pengguna;

        if($userLevel == '1') {

		    $rs     = MsJenisKegiatan::select('id', 'kode_jeniskegiatan', 'nama_jeniskegiatan', 'deleted_at')
		                ->get();
		    return view('master::msjeniskegiatan.index', ['rs' => $rs]);

	    } else {

            $rs     = MsJenisKegiatan::select('id', 'kode_jeniskegiatan', 'nama_jeniskegiatan', 'deleted_at')
                        ->where('deleted_at','=', NULL)
                        ->get();

            return view('master::msjeniskegiatan.index', ['rs' => $rs]);

        }

	}

	public function page_add() {

        $rs     = MsJenisKegiatan::select('id', 'kode_jeniskegiatan', 'nama_jeniskegiatan', 'deleted_at')
	                    		->get();

        return view('master::msjeniskegiatan.add', ['rs' => $rs]);

    }

    public function store(Request $request) {

        $check      = MsJenisKegiatan::where('kode_jeniskegiatan', $request->kode_jeniskegiatan)->first();
        $check2     = MsJenisKegiatan::onlyTrashed()->where('kode_jeniskegiatan', $request->kode_jeniskegiatan)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        $rs                       	= new MsJenisKegiatan();
        $rs->kode_jeniskegiatan		= $request->kode_jeniskegiatan;
        $rs->nama_jeniskegiatan 	= $request->nama_jeniskegiatan;
        $rs->save();

        return response()->json(['status' => 'OK']);

    }

    public function page_show($id) {

		$rs 		= MsJenisKegiatan::findOrfail($id);

        return view('master::msjeniskegiatan.show', ['rs' => $rs]);

	}

	public function edit(Request $request) {

		$kode_jeniskegiatan            = $request->kode_jeniskegiatan;
        $kode_jeniskegiatanlama  		= $request->kode_jeniskegiatanlama;


		if ($kode_jeniskegiatanlama != $kode_jeniskegiatan) {
        	$check 		= MsJenisKegiatan::where('kode_jeniskegiatan', $request->kode_jeniskegiatan)->first();
        	$check2 	= MsJenisKegiatan::onlyTrashed()->where('kode_jeniskegiatan', $request->kode_jeniskegiatan)->first();

        	if (!empty($check)) {
        		return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        	}
        	elseif (!empty($check2)) {
				return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
			}
        }

		$rs 						= MsJenisKegiatan::find($request->id);
		$rs->kode_jeniskegiatan 			= $request->kode_jeniskegiatan;
		$rs->nama_jeniskegiatan 			= $request->nama_jeniskegiatan;
		$rs->save();

		return response()->json(['status' => 'OK']);
	}

	public function delete($id) {

        $rs = MsJenisKegiatan::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

    	$rs = MsJenisKegiatan::onlyTrashed()->get();

    	return view('master::msjeniskegiatan.trash', ['rs' => $rs]);

	}

	public function restore($id)
	{
	    	$rs = MsJenisKegiatan::onlyTrashed()->where('id',$id);
	    	$rs->restore();

	    	return response()->json(['status' => 'OK']);
	}


	public function deletepermanent($id)
	{
	    	$rs = MsJenisKegiatan::onlyTrashed()->where('id',$id);
	    	$rs->forceDelete();

	    	return response()->json(['status' => 'OK']);
	}
}
