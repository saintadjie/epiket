<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\MsSubdirektorat;
use App\Models\MsSeksi;

use App\User;

use DB;
use Hash;
use Auth;
use DataTables;

class MsSubdirektoratController extends Controller
{
	public function index(Request $request) {
		$userLevel          = Auth::user()->level_pengguna;

        if($userLevel == '1') {

		    $rs     = MsSubdirektorat::select('id', 'kode_subdirektorat', 'nama_subdirektorat', 'deleted_at')
		                ->get();
		    return view('master::mssubdirektorat.index', ['rs' => $rs]);

	    } else {

            $rs     = MsSubdirektorat::select('id', 'kode_subdirektorat', 'nama_subdirektorat', 'deleted_at')
                        ->where('deleted_at','=', NULL)
                        ->get();

            return view('master::mssubdirektorat.index', ['rs' => $rs]);

        }

	}

	public function page_add() {

        $rs     = MsSubdirektorat::select('id', 'kode_subdirektorat', 'nama_subdirektorat')
	                    ->get();

        return view('master::mssubdirektorat.add', ['rs' => $rs]);

    }

    public function store(Request $request) {

        $check      = MsSubdirektorat::where('kode_subdirektorat', $request->kode_subdirektorat)->first();
        $check2     = MsSubdirektorat::onlyTrashed()->where('kode_subdirektorat', $request->kode_subdirektorat)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        $rs                       	= new MsSubdirektorat();
        $rs->kode_subdirektorat		= $request->kode_subdirektorat;
        $rs->nama_subdirektorat 	= $request->nama_subdirektorat;
        $rs->save();

        return response()->json(['status' => 'OK']);

    }

    public function page_show($id) {

		$rs 	= MsSubdirektorat::findOrfail($id);
		
		return view('master::mssubdirektorat.show', ['rs' => $rs]);

	}


	public function edit(Request $request) {

		$kode_subdirektorat            = $request->kode_subdirektorat;
        $kode_subdirektoratlama  		= $request->kode_subdirektoratlama;


		if ($kode_subdirektoratlama != $kode_subdirektorat) {
        	$check 		= MsSubdirektorat::where('kode_subdirektorat', $request->kode_subdirektorat)->first();
        	$check2 	= MsSubdirektorat::onlyTrashed()->where('kode_subdirektorat', $request->kode_subdirektorat)->first();

        	if (!empty($check)) {
        		return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        	}
        	elseif (!empty($check2)) {
				return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
			}
        }

		$rs 						= MsSubdirektorat::find($request->id);
		$rs->kode_subdirektorat 	= $request->kode_subdirektorat;
		$rs->nama_subdirektorat 	= $request->nama_subdirektorat;
		$rs->save();

		return response()->json(['status' => 'OK']);
	}


    public function delete($id) {

        $rs = MsSubdirektorat::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

    	$rs = MsSubdirektorat::onlyTrashed()->get();

    	return view('master::mssubdirektorat.trash', ['rs' => $rs]);

	}

	public function restore($id)
	{
	    	$rs = MsSubdirektorat::onlyTrashed()->where('id',$id);
	    	$rs->restore();

	    	return response()->json(['status' => 'OK']);
	}


	public function deletepermanent($id)
	{
	    	$rs = MsSubdirektorat::onlyTrashed()->where('id',$id);
	    	$rs->forceDelete();

	    	return response()->json(['status' => 'OK']);
	}

}
