<?php

namespace App\Modules\Master\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pegawai;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;

use App\User;

use DB;
use Hash;
use Image;
use File;
use Auth;
use DataTables;

class PegawaiController extends Controller
{

	public function index() {

        $userLevel          = Auth::user()->level_pengguna;
        $userSubdirektorat  = Auth::user()->kode_subdirektorat;
        $userNip            = Auth::user()->nip;

        if($userLevel == '1' && $userNip == 'superadmin') {

            $rs     = User::select('id', 'nip', 'nama', 'kontak', 'kode_subdirektorat', 'kode_seksi', 'level_pengguna', 'deleted_at')
                        
                        ->get();
            $hitungpegawai = count($rs);

            return view('master::pegawai.index', ['rs' => $rs, 'hitungpegawai' => $hitungpegawai]);

        } else {

            $rs     = User::select('id', 'nip', 'nama', 'kontak', 'kode_subdirektorat', 'kode_seksi', 'level_pengguna', 'deleted_at')
                        ->where('kode_subdirektorat','=', $userSubdirektorat)
                        ->where('level_pengguna','<>','1')
                        ->get();
            $hitungpegawai = count($rs);

            return view('master::pegawai.index', ['rs' => $rs, 'hitungpegawai' => $hitungpegawai]);

        }

    }


    public function page_add() {

        $rs         = User::select('id', 'nip', 'nama', 'kode_subdirektorat', 'level_pengguna')
                        ->get();

        return view('master::pegawai.add', ['rs' => $rs]);

    }

    public function store(Request $request) {

        $path       = null;
        $check      = Pengguna::where('nip', $request->nip)->first();
        $check2     = Pengguna::onlyTrashed()->where('nip', $request->nip)->first();

        if (!empty($check)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
        }

        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
        } 

        elseif ($request->hasFile('photo')) {
            $dir = public_path() . "/files/foto_pengguna/$request->nip";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            Image::make($request->file('photo'))->resize(150, 150)->save($dir . '/' . $request->nip . '.jpg', 100);
            $path = 'files/foto_pengguna/' . $request->nip . '/' . $request->nip . '.jpg';
        }

        $user                       = new User();
        $user->nip                  = $request->nip;
        $user->nama                 = $request->nama;
        $user->password             = Hash::make($request->password)    ;
        $user->kode_subdirektorat   = $request->kode_subdirektorat;
        $user->level_pengguna       = $request->level_pengguna;
        $user->photo                = $path != null ? $path : null; 
        $user->save();

        switch ($request->level_pengguna) {
            case 1:
                $role = 'SUPERADMIN';
            break;

            case 2:
                $role = 'PEGAWAI';
            break;
        }

        $user->assignRole($role);

        return response()->json(['status' => 'OK']);

    }

    public function page_profile() {

        $userID     = Auth::user()->id;
        $userLevel  = Auth::user()->level_pengguna;
        
        $rs         = User::findOrfail($userID);

        return view('master::pegawai.profile', ['rs' => $rs]);
        
    }

    public function edit(Request $request) {

        $userID     = Auth::user()->id;
        $path       = null;
        $nip        = $request->nip;
        $niplama    = $request->niplama;

        if ($niplama != $nip) {
            $check      = User::where('nip', $request->nip)->first();
            $check2     = Pengguna::onlyTrashed()->where('nip', $request->nip)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
            elseif (!empty($check2)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
            }
        }

        if ($request->hasFile('photo')) {
            $dir = public_path() . "/files/foto_pengguna/$request->nip";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            Image::make($request->file('photo'))->resize(150, 150)->save($dir . '/' . $request->nip . '.jpg', 100);
            $path = 'files/foto_pengguna/' . $request->nip . '/' . $request->nip . '.jpg';
        }

        $user                       = User::find($userID);
        $path                       = $user->photo != null && $path == null ? $user->photo : $path; 
        $user->nip                  = $request->nip;
        $user->nama                 = $request->nama;
        $user->level_pengguna       = $request->level_pengguna;
        $user->kode_subdirektorat   = $request->kode_subdirektorat;
        $user->photo                = $path != null ? $path : null;
        $user->save();


        switch ($request->level_pengguna) {
            case 1:
                $role = 'SUPERADMIN';
            break;

            case 2:
                $role = 'PEGAWAI';
            break;
        }

        $user->syncRoles($role);

        return response()->json(['status' => 'OK']);
    }


    public function page_show($id) {

        $userID     = Auth::user()->id;
        $userLevel  = Auth::user()->level_pengguna;
        $rsRole     = User::findOrfail($id)->level_pengguna;
         
        if($userLevel == '1') {

            if($userID == $id && $rsRole == '1' ) {

                $rs         = Pengguna::findOrfail($id);

                return view('master::pegawai.show', ['rs' => $rs]);

            } else if($rsRole != '1' ) {

                $rs         = User::findOrfail($id);

                return view('master::pegawai.show', ['rs' => $rs]);

            } else {

                abort(404);

            }

        } else if($userID != $id) {

                abort(404);

        } else if($userLevel == '2') {

            $rs         = User::findOrfail($id);

            return view('master::pegawai.show', ['rs' => $rs]);

        }
    }

    public function editshow(Request $request) {
        
        $path       = null;
        $nip        = $request->nip;
        $niplama    = $request->niplama;

        if ($niplama != $nip) {
            $check      = User::where('nip', $request->nip)->first();
            $check2     = Pengguna::onlyTrashed()->where('nip', $request->nip)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Duplicate' ] );
            }
            elseif (!empty($check2)) {
                return response()->json( [ 'status' => 'Failed', 'message' => 'Trash' ] );
            }
        }

        if ($request->hasFile('photo')) {
            $dir = public_path() . "/files/foto_pengguna/$request->nip";
            if (!is_dir($dir)) {
                File::makeDirectory($dir, $mode = 0777, true, true);         
            }
            Image::make($request->file('photo'))->resize(150, 150)->save($dir . '/' . $request->nip . '.jpg', 100);
            $path = 'files/foto_pengguna/' . $request->nip . '/' . $request->nip . '.jpg';
        }

        $user                       = User::find($request->id);
        $path                       = $user->photo != null && $path == null ? $user->photo : $path; 
        $user->nip                  = $request->nip;
        $user->nama                 = $request->nama;
        $user->level_pengguna       = $request->level_pengguna;
        $user->kode_subdirektorat   = $request->kode_subdirektorat;
        $user->photo                = $path != null ? $path : null;
        $user->save();


        switch ($request->level_pengguna) {
            case 1:
                $role = 'SUPERADMIN';
            break;

            case 2:
                $role = 'PEGAWAI';
            break;
        }

        $user->syncRoles($role);

        return response()->json(['status' => 'OK']);

    }


    public function nonaktif($id) {

        $rs                 = Pengguna::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function aktif($id)
    {
            $rs = Pengguna::onlyTrashed()->where('id',$id);
            $rs->restore();

            return response()->json(['status' => 'OK']);
    }

    public function ubahPassword(Request $request) {

        if (!(Hash::check($request->sandi_lama, Auth::user()->password))) {
            return response()->json(['status' => 'Failed', 'message' => 'Kata sandi lama tidak cocok']);
        }
 
        if(strcmp($request->sandi_baru, $request->sandi_lama) == 0){
            return response()->json(['status' => 'Failed', 'message' => 'Kata sandi baru tidak boleh sama dengan kata sandi lama']);
        }

        $user           = Auth::user();
        $user->password = Hash::make($request->sandi_baru);
        $user->save();
        return response()->json(['status' => 'OK']);
	}

    public function ubahPasswordAdmin(Request $request) {

        $rs                = Pengguna::findOrfail($request->id);
        $rs->password      = Hash::make($request['password_baru']);
        $rs->save();

        return response()->json(['status' => 'OK']);
    }

    public function delete($id) {

        $rs                   = Pengguna::findOrfail($id);
        $rs->delete();

        return response()->json(['status' => 'OK']);

    }

    public function trash() {

        $rs = Pengguna::onlyTrashed()->get();

        return view('master::pengguna.trash', ['rs' => $rs]);

    }

    public function restore($id)
    {
        $rs = Pengguna::onlyTrashed()->where('id',$id);
        $rs->restore();

        return response()->json(['status' => 'OK']);
    }

}
