@extends('layouts.home')
@push('script-header')
        
@endpush
@section('content')

    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item"><a href="{{url('master/subdirektorat')}}">Master Subdirektorat</a></li>
                        <li class="breadcrumb-item active">Ubah Subdirektorat</li>
                    </ol>
                </div>

                <div class="col s2 m6 l6">
                    <a onclick="history.back()" class="waves-effect waves-light  btn pink accent-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">undo</i> Kembali
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12 m6 l6">
        <div id="placeholder" class="card card card-default scrollspy">
            <div class="card-content">
                <form id="form_subdirektorat">
                    <div class="row">
                        <div class="input-field col s12" hidden>
                            <input placeholder="Kode Subdirektorat" id="kode_subdirektoratlama" name="kode_subdirektoratlama" type="text" value="{{$rs->kode_subdirektorat}}" disabled>
                            <label for="kode_subdirektoratlama">Kode Subdirektorat</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Kode Subdirektorat" id="kode_subdirektorat" name="kode_subdirektorat" type="text" value="{{$rs->kode_subdirektorat}}">
                            <label for="kode_subdirektorat">Kode Subdirektorat</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Nama Subdirektorat" id="nama_subdirektorat" name="nama_subdirektorat" type="text" value="{{$rs->nama_subdirektorat}}">
                            <label for="nama_subdirektorat">Nama Subdirektorat</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn gradient-45deg-purple-deep-orange waves-effect waves-light right" type="button" id="btn_simpan">Simpan
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@push('script-footer')
    <script src="{{url('js/master/mssubdirektorat/show_app.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#idBtnMaster').addClass('active bold open');
            $("#bodyMaster").css("display", "block");
            $('#btnMasterSubdirektorat').addClass('waves-effect waves-cyan active');
        });
    </script>

    <script type="text/javascript">
        var id = "{{$rs->id}}"
        var url_api = "{{url('api/v1/master/subdirektorat/edit')}}"
        var url_subdirektorat = "{{url('/master/subdirektorat/')}}"
    </script>
@endpush
@endsection


