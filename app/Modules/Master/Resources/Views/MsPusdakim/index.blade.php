@extends('layouts.home')
@push('script-header')
    <!-- DataTables -->
    <link href="{{url('assets/vendors/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css/data-tables.min.css')}}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .ellipsis {
            position: relative;
        }
        .ellipsis:before {
            content: '&nbsp;';
            visibility: hidden;
        }
        .ellipsis span {
            position: absolute;
            left: 0;
            right: 0;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
@endpush

@section('content')
    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s6 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item active">Master Pusdakim</li>
                    </ol>
                </div>

                <div class="col s6 m6 l6">
                    <a href="{{url('master/pusdakim/add')}}" class="waves-effect waves-light  btn gradient-45deg-purple-deep-orange box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">playlist_add</i> Tambah Pusdakim
                    </a>
                    @can('Melihat data mspusdakim terhapus')
                    <a href="{{url('master/pusdakim/trash')}}" class="waves-effect waves-light  btn grey darken-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">restore</i> Kembalikan data
                    </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12">
        <div class="container">
            <!-- invoice list -->
            <section class="section section-data-tables ">

                <!-- Page Length Options -->
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12">
                                        <table id="page-length-option" class="display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align: middle; text-align: center;">Kode Pusdakim</th>
                                                    <th style="vertical-align: middle; text-align: center;">Nama Pusdakim</th>
                                                    <th style="vertical-align: middle; text-align: center;">Lokasi</th>
                                                    <th style="vertical-align: middle; text-align: center;">Keterangan</th>
                                                    @can('Mengubah data mspusdakim')
                                                    <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                                    @endcan
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($rs as $result)
                                                <tr id="{{$result->id}}">
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        <span>{{$result->kode_pusdakim}}</span>
                                                    </td>
                                                    <td style="vertical-align: middle; text-align: center;"><span>{{$result->nama_pusdakim}}</span></td>
                                                    <td style="vertical-align: middle; text-align: center;"><span>{{$result->lokasi_pusdakim}}</span></td>
                                                    <td style="vertical-align: middle; text-align: center;" class="ellipsis"><span>{{$result->keterangan}}</span></td>
                                                    @can('Mengubah data mspusdakim')
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        <div class="invoice-action">
                                                            <a class="waves-effect waves-light light-blue darken-4 btn-small border-round mb-1 mr-1" href="{{url('/master/pusdakim/show/'.$result->id)}}" id="btn_edit"><i class="material-icons left">edit</i>Ubah</a>
                                                            <a class="waves-effect waves-light pink accent-3 btn-small border-round mb-1 mr-1" data-kodepusdakim="{{$result->kode_pusdakim}}" id="btn_hapus"><i class="material-icons left">delete</i>Hapus</a>
                                                        </div>
                                                    </td>
                                                    @endcan
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@push('script-footer')
    <script src="{{url('assets/vendors/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('assets/vendors/js/data-tables.min.js')}}"></script>
    <script src="{{url('js/master/mspusdakim/index_app.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#idBtnMaster').addClass('active bold open');
            $("#bodyMaster").css("display", "block");
            $('#btnMasterPusdakim').addClass('waves-effect waves-cyan active');
        });
    </script>

    <script type="text/javascript">
        var url_mspusdakim     = "{{url('/master/pusdakim/')}}"
        var url_delete         = "{{url('api/v1/master/pusdakim/delete')}}"
    </script>
@endpush
@endsection