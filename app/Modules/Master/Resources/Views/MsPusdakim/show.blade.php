@extends('layouts.home')
@push('script-header')
        
@endpush
@section('content')

    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item"><a href="{{url('master/pusdakim')}}">Master Pusdakim</a></li>
                        <li class="breadcrumb-item active">Ubah Pusdakim</li>
                    </ol>
                </div>

                <div class="col s2 m6 l6">
                    <a onclick="history.back()" class="waves-effect waves-light  btn pink accent-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">undo</i> Kembali
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12 m6 l6">
        <div id="placeholder" class="card card card-default scrollspy">
            <div class="card-content">
                <form id="form_pusdakim">
                    <div class="row">
                        <div class="input-field col s12" hidden>
                            <input placeholder="Kode Pusdakim" id="kode_pusdakimlama" name="kode_pusdakimlama" type="text" value="{{$rs->kode_pusdakim}}" disabled>
                            <label for="kode_pusdakimlama">Kode Pusdakim</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Kode Pusdakim" id="kode_pusdakim" name="kode_pusdakim" type="text" value="{{$rs->kode_pusdakim}}">
                            <label for="kode_pusdakim">Kode Pusdakim</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Nama Pusdakim" id="nama_pusdakim" name="nama_pusdakim" type="text" value="{{$rs->nama_pusdakim}}">
                            <label for="nama_pusdakim">Nama Pusdakim</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Lokasi Pusdakim" id="lokasi_pusdakim" name="lokasi_pusdakim" type="text" value="{{$rs->lokasi_pusdakim}}">
                            <label for="lokasi_pusdakim">Lokasi Pusdakim</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea placeholder="Keterangan" id="keterangan" name="keterangan" class="materialize-textarea">{{$rs->keterangan}}</textarea>
                            <label for="keterangan" class="">Keterangan</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn gradient-45deg-purple-deep-orange waves-effect waves-light right" type="button" id="btn_simpan">Simpan
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@push('script-footer')
    <script src="{{url('js/master/mspusdakim/show_app.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#idBtnMaster').addClass('active bold open');
            $("#bodyMaster").css("display", "block");
            $('#btnMasterpusdakim').addClass('waves-effect waves-cyan active');
        });
    </script>

    <script type="text/javascript">
        var id = "{{$rs->id}}"
        var url_api = "{{url('api/v1/master/pusdakim/edit')}}"
        var url_pusdakim = "{{url('/master/pusdakim/')}}"
    </script>
@endpush
@endsection


