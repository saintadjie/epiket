@extends('layouts.home')
@push('script-header')
    <!-- DataTables -->
    <link href="{{url('assets/vendors/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css/data-tables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush

@section('content')
    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item active">Master Pegawai</li>
                    </ol>
                </div>

                <div class="col s6 m6 l6">
                    <a href="{{url('master/pegawai/add')}}" class="waves-effect waves-light  btn gradient-45deg-purple-deep-orange box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">person_add</i> Tambah Pegawai
                    </a>
                    @can('Melihat data pegawai terhapus')
                    <a href="{{url('master/subdirektorat/trash')}}" class="waves-effect waves-light  btn grey darken-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">restore</i> Kembalikan data
                    </a>
                    @endcan
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12">
        <div class="container">
            <!-- invoice list -->
            <section class="section section-data-tables ">

                <!-- Page Length Options -->
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12">
                                        <table id="page-length-option" class="display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align: middle; text-align: center;">NIP</th>
                                                    <th style="vertical-align: middle; text-align: center;">Nama</th>
                                                    <th style="vertical-align: middle; text-align: center;">Seksi</th>
                                                    <th style="vertical-align: middle; text-align: center;">Kontak</th>
                                                    <th style="vertical-align: middle; text-align: center;">Level</th>
                                                    <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($rs as $result)
                                                <tr>
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        <span>{{$result->nip}}</span>
                                                    </td>
                                                    <td style="vertical-align: middle; text-align: center;"><span>{{$result->nama}}</span></td>
                                                    <td style="vertical-align: middle; text-align: center;"><small>{{$result->kode_seksi}}</small></td>
                                                    <td style="vertical-align: middle; text-align: center;"><span>{{$result->kontak}}</span></td>
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        @if($result->level_pengguna == 1) 
                                                            <span class="chip lighten-5 red red-text">SUPERADMIN</span> 
                                                        @elseif($result->level_pengguna == 2 ) 
                                                            <h5><span class="badge badge-pill badge-primary">PENGGUNA</span></h5>
                                                        @endif
                                                    </td>
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        <div class="invoice-action">
                                                            <a href="app-invoice-view.html" class="invoice-action-view mr-4">
                                                                <i class="material-icons">remove_red_eye</i>
                                                            </a>
                                                            <a href="app-invoice-edit.html" class="invoice-action-edit">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@push('script-footer')
    <script src="{{url('assets/vendors/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('assets/vendors/js/data-tables.min.js')}}"></script>
    <script src="{{url('js/master/pegawai/index_app.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#idBtnMaster').addClass('active bold open');
            $("#bodyMaster").css("display", "block");
            $('#btnMasterPegawai').addClass('waves-effect waves-cyan active');
        });
    </script>

    <script type="text/javascript">
        var url_pegawai    = "{{url('/master/pegawai/')}}"
        var url_nonaktif   = "{{url('api/v1/master/pegawai/nonaktif')}}"
        var url_aktif      = "{{url('/master/pegawai/aktif')}}"
    </script>
@endpush
@endsection