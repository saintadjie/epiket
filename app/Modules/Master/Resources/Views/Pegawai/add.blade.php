@extends('layouts.home')
@push('script-header')
        <link href="{{url('assets/libs/fileinput/css/fileinput.min.css')}}" rel="stylesheet" />
@endpush
@section('content')

    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item"><a href="{{url('master/pegawai')}}">Master Pegawai</a></li>
                        <li class="breadcrumb-item active">Tambah Pegawai</li>
                    </ol>
                </div>

                <div class="col s2 m6 l6">
                    <a href="{{url('master/pegawai/add')}}" class="waves-effect waves-light  btn gradient-45deg-light-blue-cyan box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">person_add</i> Tambah Pegawai
                    </a>
              </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12 m6 l6">
        <div id="placeholder" class="card card card-default scrollspy">
            <div class="card-content">
                <form id="form_pegawai">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="NIP" id="nip" type="text" onkeypress="return isNumeric(event)" maxlength="18">
                            <label for="nip">NIP</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Nama" id="nama" type="text">
                            <label for="nama">Nama</label>
                        </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input placeholder="john@domainname.com" id="email2" type="email">
                        <label for="email">Email</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input placeholder="YourPassword" id="password2" type="password">
                        <label for="password">Password</label>
                      </div>
                    </div>
                    <textarea class="form-control" id="xyz" name="xyz" maxlength="10"></textarea>
            <div class="row">
              <div class="input-field col s12">
                <textarea placeholder="Oh WoW! Let me check this one too." id="message2"
                  class="materialize-textarea"></textarea>
                <label for="message">Message</label>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

@push('script-footer')
        <script src="{{url('js/aplikasi/pegawai/add_app.js')}}"></script>
        <script src="{{url('assets/vendors/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script type="text/javascript">
            var url_api = "{{url('api/v1/aplikasi/pegawai/store')}}"

            $('#nip').maxlength({
                alwaysShow: true,
                threshold: 10,
                warningClass: "label label-info",
                limitReachedClass: "label label-warning",
                placement: 'bottom-right-inside',
                preText: 'Menggunakan ',
                separator: ' dari ',
                postText: ' karakter.'
            });
        </script>

        <script type="text/javascript">

            function isNumeric(evt) {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }
        </script>
@endpush
@endsection


