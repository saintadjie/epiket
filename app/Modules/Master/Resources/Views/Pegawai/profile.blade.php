@extends('layouts.layout')
@push('script-header')
        <link href="{{url('assets/libs/fileinput/css/fileinput.min.css')}}" rel="stylesheet" />
@endpush
@section('content')

    <!-- Page-Title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h4 class="page-title mb-1">Aplikasi</h4>
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{url('aplikasi/pegawai')}}">Pengguna</a></li>
                        <li class="breadcrumb-item active">Profil</li>
                    </ol>
                </div>
                <div class="col-md-4">
                    <div class="float-right d-none d-md-block">
                        <div class="dropdown">
                            <button onclick="history.back()" class="btn btn-light btn-rounded" type="button" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-undo mr-1"></i> Kembali
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title end breadcrumb -->

    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-7">
                                    <h5>
                                        <?php
                                            date_default_timezone_set('Asia/Jakarta');
                                            $a = date ("H");
                                            if (($a>=6) && ($a<=11)){
                                            echo "Selamat Pagi !";
                                            }
                                            else if(($a>11) && ($a<=15))
                                            {
                                            echo "Selamat Siang !";}
                                            else if (($a>15) && ($a<=18)){
                                            echo "Selamat Sore !";}
                                            else { echo "Selamat Malam";}
                                        ?>
                                        
                                    </h5>
                                    <div class="mail-list mt-4">
                                        <a><i class="fas fa-shield-alt mr-2"></i> {{ $rs->nip }}</a>
                                        <a><i class="fas fa-user mr-2"></i> {{ $rs->nama }}</a>
                                        <a>
                                            <i class="fas fa-folder mr-2"></i>
                                            @if($rs->level_pengguna == 1) 
                                                SUPERADMIN</span>
                                            @elseif($rs->level_pengguna == 2 ) 
                                                PENGGUNA
                                            @endif
                                        </a>
                                        <a><i class="fas fa-building mr-2"></i>{{ $rs->kode_subdirektorat }}</a>
                                    </div>
                                </div>

                                <div class="col-5 ml-auto">
                                    <div>
                                        @if(Auth::user()->photo != null)
                                            <img class="img-fluid" src="{{url(Auth::user()->photo)}}" alt="User" height="150">
                                        @else
                                            <img class="img-fluid" src="{{url('assets\images\user.png')}}" alt="User" height="150">
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">UBAH DETAIL PENGGUNA</h4>
                            <p class="card-title-desc">Mengubah detail Pengguna</p>
                            <form id="form_pegawai">
                                <div class="form-group row">
                                    <label for="nip" class="col-md-2 col-form-label">NIP</label>
                                    <div class="col-md-6">
                                        <input type="text" onkeypress="return isNumeric(event)" class="form-control" id="nip" name="nip" placeholder="NIP" maxlength="18" value="{{ $rs->nip }}">
                                    </div>
                                </div>
                                <div class="form-group row" hidden>
                                    <label for="niplama" class="col-md-2 col-form-label">NIP Lama</label>
                                    <div class="col-md-6">
                                        <input type="number" class="form-control" id="niplama" name="niplama" placeholder="NIP" value="{{ $rs->nip }}" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nama" class="col-md-2 col-form-label">Nama</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" value="{{ $rs->nama }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="level_pengguna" class="col-md-2 col-form-label">Hak Akses</label>
                                    <div class="col-md-6">
                                        <select class="form-control show-tick" id="level_pengguna" name="level_pengguna">
                                            <option value="pilih" disabled selected>-- Pilih Level --</option>
                                            @if(Auth::user()->id == 1)
                                            <option value="1">SUPERADMIN</option>
                                            <option value="2">PENGGUNA</option>
                                            @else
                                            <option value="2">PENGGUNA</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kode_subdirektorat" class="col-md-2 col-form-label">Direktorat</label>
                                    <div class="col-md-6">
                                        <select class="form-control show-tick" id="kode_subdirektorat" name="kode_subdirektorat">
                                            <option value="pilih" disabled selected>-- Pilih Direktorat --</option>
                                            <option value="INTAL">INTAL</option>
                                            <option value="SISTIK">SISTIK</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="photo" class="col-md-2 col-form-label">Foto</label>
                                    <div class="col-md-6">
                                        <div class="custom-file">
                                            <input type="file" name="photo" id="photo">
                                            
                                        </div>
                                        <hr/>
                                        <i> Diupload dengan format PNG dan JPG Max 500 Kb </i>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="photo" class="col-md-2 col-form-label"></label>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-rounded waves-effect waves-light fas fa-save float-right" id="btn_simpan"> Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row -->

        </div>
        <!-- end container-fluid -->
    </div> 
    <!-- end page-content-wrapper -->
@push('script-footer')
        <script src="{{url('assets/libs/fileinput/js/plugins/piexif.min.js')}}"></script>
        <script src="{{url('assets/libs/fileinput/js/fileinput.min.js')}}"></script>
        <script src="{{url('assets/libs/fileinput/js/locales/id.js')}}"></script>
        <script src="{{url('js/aplikasi/pegawai/profile_app.js')}}"></script>
        <script src="{{url('assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>

        <script type="text/javascript">
            var id          = "{{$rs->id}}"
            var url_api     = "{{url('api/v1/aplikasi/pegawai/edit')}}"
            var url_apipass = "{{url('api/v1/aplikasi/pegawai/ubahPasswordAdmin')}}"
            var url_profile = "{{url('/aplikasi/pegawai/profile')}}"
            var url_depan   = "{{url('/aplikasi/pegawai')}}"

            $(document).ready(function(){
                $('#level_pengguna').val("{{$rs->level_pengguna}}").change();
                $('#kode_subdirektorat').val("{{$rs->kode_subdirektorat}}").change();
            })

            $('#nip').maxlength({
                alwaysShow: true,
                threshold: 10,
                warningClass: "badge badge-danger",
                limitReachedClass: "badge badge-primary",
                placement: 'bottom-right-inside',
                message: 'Menggunakan %charsTyped% dari %charsTotal% digit.'
            });

        </script>

        <script type="text/javascript">

            function isNumeric(evt) {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[0-9]|\./;
                if (!regex.test(key)) {
                    theEvent.returnValue = false;
                    if (theEvent.preventDefault) theEvent.preventDefault();
                }
            }
        </script>
@endpush
@endsection


