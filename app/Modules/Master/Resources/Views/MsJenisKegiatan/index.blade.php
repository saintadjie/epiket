@extends('layouts.home')
@push('script-header')
    <!-- DataTables -->
    <link href="{{url('assets/vendors/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css/data-tables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush

@section('content')
    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item active">Master Jenis Kegiatan</li>
                    </ol>
                </div>

                <div class="col s2 m6 l6">
                    <a href="{{url('master/jeniskegiatan/add')}}" class="waves-effect waves-light  btn gradient-45deg-purple-deep-orange box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">local_activity</i> Tambah Jenis Kegiatan
                    </a>
                    @can('Melihat data msjeniskegiatan terhapus')
                    <a href="{{url('master/jeniskegiatan/trash')}}" class="waves-effect waves-light  btn grey darken-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">restore</i> Kembalikan data
                    </a>
                    @endcan
              </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12">
        <div class="container">
            <!-- invoice list -->
            <section class="section section-data-tables ">

                <!-- Page Length Options -->
                <div class="row">
                    <div class="col s12">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12">
                                        <table id="page-length-option" class="display nowrap">
                                            <thead>
                                                <tr>
                                                    <th style="vertical-align: middle; text-align: center;">Kode Jenis Kegiatan</th>
                                                    <th style="vertical-align: middle; text-align: center;">Nama Jenis Kegiatan</th>
                                                    @can('Mengubah data msjeniskegiatan')
                                                    <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                                    @endcan
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($rs as $result)
                                                <tr id="{{$result->id}}">
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        <span>{{$result->kode_jeniskegiatan}}</span>
                                                    </td>
                                                    <td style="vertical-align: middle; text-align: center;"><span>{{$result->nama_jeniskegiatan}}</span></td>
                                                    @can('Mengubah data msjeniskegiatan')
                                                    <td style="vertical-align: middle; text-align: center;">
                                                        <div class="invoice-action">
                                                            <a class="waves-effect waves-light light-blue darken-4 btn-small border-round mb-1 mr-1" href="{{url('/master/jeniskegiatan/show/'.$result->id)}}" id="btn_edit"><i class="material-icons left">edit</i>Ubah</a>
                                                            <a class="waves-effect waves-light pink accent-3 btn-small border-round mb-1 mr-1" data-kodejeniskegiatan="{{$result->kode_jeniskegiatan}}" id="btn_hapus"><i class="material-icons left">delete</i>Hapus</a>
                                                        </div>
                                                    </td>
                                                    @endcan
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@push('script-footer')
    <script src="{{url('assets/vendors/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('assets/vendors/js/data-tables.min.js')}}"></script>
    <script src="{{url('js/master/msjeniskegiatan/index_app.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#idBtnMaster').addClass('active bold open');
            $("#bodyMaster").css("display", "block");
            $('#btnMasterJenisKegiatan').addClass('waves-effect waves-cyan active');
        });
    </script>

    <script type="text/javascript">
        var url_msjeniskegiatan   = "{{url('/master/jeniskegiatan/')}}"
        var url_delete          = "{{url('api/v1/master/jeniskegiatan/delete')}}"
    </script>
@endpush
@endsection