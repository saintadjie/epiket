@extends('layouts.home')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/select2/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/select2/form-select2.min.css')}}"/>  
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/select2/select2-materialize.css')}}"/>     
@endpush
@section('content')

    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item"><a href="{{url('master/jeniskegiatan')}}">Master Jenis Kegiatan</a></li>
                        <li class="breadcrumb-item active">Ubah Jenis Kegiatan</li>
                    </ol>
                </div>

                <div class="col s2 m6 l6">
                    <a onclick="history.back()" class="waves-effect waves-light  btn pink accent-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">undo</i> Kembali
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12 m6 l6">
        <div id="placeholder" class="card card card-default scrollspy">
            <div class="card-content">
                <form id="form_jeniskegiatan">
                    <div class="row">
                        <div class="input-field col s12" hidden>
                            <input placeholder="Kode Jenis Kegiatan" id="kode_jeniskegiatanlama" name="kode_jeniskegiatanlama" type="text" value="{{$rs->kode_jeniskegiatan}}" disabled>
                            <label for="kode_jeniskegiatanlama">Kode Jenis Kegiatan</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Kode Jenis Kegiatan" id="kode_jeniskegiatan" name="kode_jeniskegiatan" type="text" value="{{$rs->kode_jeniskegiatan}}">
                            <label for="kode_jeniskegiatan">Kode Jenis Kegiatan</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Nama Jenis Kegiatan" id="nama_jeniskegiatan" name="nama_jeniskegiatan" type="text" value="{{$rs->nama_jeniskegiatan}}">
                            <label for="nama_jeniskegiatan">Nama Jenis Kegiatan</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn gradient-45deg-purple-deep-orange waves-effect waves-light right" type="button" id="btn_simpan">Simpan
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

@push('script-footer')
    <script src="{{url('assets/vendors/select2/select2.full.min.js')}}"></script>
    <script src="{{url('assets/vendors/select2/form-select2.min.js')}}"></script>
    <script src="{{url('js/master/msjeniskegiatan/show_app.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#idBtnMaster').addClass('active bold open');
            $("#bodyMaster").css("display", "block");
            $('#btnMasterJenisKegiatan').addClass('waves-effect waves-cyan active');
        });
    </script>

    <script type="text/javascript">
        var id = "{{$rs->id}}"
        var url_api = "{{url('api/v1/master/jeniskegiatan/edit')}}"
        var url_jeniskegiatan = "{{url('/master/jeniskegiatan/')}}"
    </script>
@endpush
@endsection


