@extends('layouts.home')
@push('script-header')
    <!-- DataTables -->
    <link href="{{url('assets/vendors/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/vendors/css/data-tables.min.css')}}" rel="stylesheet" type="text/css" />

@endpush
@section('content')
<div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
    <div class="container">
        <div class="row">
            
            <div class="col s6 m6 l6 breadcrumbs-left">
                <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                <ol class="breadcrumbs mb-0">
                    <li class="breadcrumb-item"><a href="{{url('master/seksi')}}">Master Seksi</a></li>
                    <li class="breadcrumb-item active">Restore Seksi</li>
                </ol>
            </div>

            <div class="col s2 m6 l6">
                <a onclick="history.back()" class="waves-effect waves-light  btn pink accent-3 box-shadow-none border-round mr-1 mb-1 right">
                    <i class="material-icons left">undo</i> Kembali
                </a>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN: Page Main-->
    
<div class="col s12">
    <div class="container">
        <!-- invoice list -->
        <section class="section section-data-tables ">

            <!-- Page Length Options -->
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12">
                                    <table id="page-length-option" class="display nowrap">
                                        <thead>
                                            <tr>
                                                <th style="vertical-align: middle; text-align: center;">Kode Seksi</th>
                                                <th style="vertical-align: middle; text-align: center;">Nama Seksi</th>
                                                <th style="vertical-align: middle; text-align: center;">Subdirektorat</th>
                                                @can('Mengubah data msseksi')
                                                <th style="vertical-align: middle; text-align: center;">Aksi</th>
                                                @endcan
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($rs as $result)
                                            <tr id="{{$result->id}}">
                                                <td style="vertical-align: middle; text-align: center;">
                                                    <span>{{$result->kode_seksi}}</span>
                                                </td>
                                                <td style="vertical-align: middle; text-align: center;"><span>{{$result->nama_seksi}}</span></td>
                                                <td style="vertical-align: middle; text-align: center;"><span>{{$result->nama_subdirektorat}}</span></td>
                                                @can('Mengubah data msseksi')
                                                <td style="vertical-align: middle; text-align: center;">
                                                    <div class="invoice-action">
                                                        <a class="waves-effect waves-light light-blue darken-4 btn-small border-round mb-1 mr-1" data-kodeseksi="{{$result->kode_seksi}}" id="btn_restore"><i class="material-icons left">restore</i>Kembalikan</a>
                                                        <a class="waves-effect waves-light pink accent-3 btn-small border-round mb-1 mr-1" data-kodeseksi="{{$result->kode_seksi}}" id="btn_hapus"><i class="material-icons left">delete_forever</i>Hapus Permanen</a>
                                                    </div>
                                                </td>
                                                @endcan
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@push('script-footer')
<script src="{{url('assets/vendors/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/vendors/js/data-tables.min.js')}}"></script>
<script src="{{url('js/master/msseksi/restore_app.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.atastombol').removeClass('active bold open');
        $('.tombol').removeClass('waves-effect waves-cyan active');
        $('#idBtnMaster').addClass('active bold open');
        $("#bodyMaster").css("display", "block");
        $('#btnMasterSeksi').addClass('waves-effect waves-cyan active');
    });
</script>

<script type="text/javascript">
    var url_trash               = "{{url('/master/seksi/trash')}}"
    var url_restore             = "{{url('/master/seksi/restore')}}"
    var url_deletepermanent     = "{{url('api/v1/master/seksi/deletepermanent')}}"
</script>
@endpush
@endsection