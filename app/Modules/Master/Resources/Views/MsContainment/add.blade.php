@extends('layouts.home')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/select2/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/select2/form-select2.min.css')}}"/>  
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/select2/select2-materialize.css')}}"/>      
@endpush
@section('content')

    <div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                
                <div class="col s10 m6 l6 breadcrumbs-left">
                    <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Master</span></h5>
                    <ol class="breadcrumbs mb-0">
                        <li class="breadcrumb-item"><a href="{{url('master/containment')}}">Master Containment</a></li>
                        <li class="breadcrumb-item active">Tambah Containment</li>
                    </ol>
                </div>

                <div class="col s2 m6 l6">
                    <a onclick="history.back()" class="waves-effect waves-light  btn pink accent-3 box-shadow-none border-round mr-1 mb-1 right">
                        <i class="material-icons left">undo</i> Kembali
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Page Main-->
    
    <div class="col s12 m6 l6">
        <div id="placeholder" class="card card card-default scrollspy">
            <div class="card-content">
                <form id="form_containment">

                    <div class="row">
                        <div class="input-field col s12">
                            <select id="kode_pusdakim" class="select2 browser-default" name="kode_pusdakim">
                                <option value="pilih" disabled selected>Pilih Pusdakim</option>
                                @foreach($rspusdakim as $rspusdakim)
                                <option value="{{$rspusdakim->kode_pusdakim}}" data-pusdakimname="{{$rspusdakim->nama_pusdakim}}">{{$rspusdakim->display}}</option>
                                @endforeach
                            </select>
                            <label>Pusdakim</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <select id="kode_ruangan" class="select2 browser-default" name="kode_ruangan" disabled>
                                <option value="pilih" disabled selected>Pilih Ruangan</option>
                                @foreach($rsruangan as $rsruangan)
                                <option value="{{$rsruangan->kode_ruangan}}" data-ruanganname="{{$rsruangan->nama_ruangan}}">{{$rsruangan->display}}</option>
                                @endforeach
                            </select>
                            <label>Ruangan</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Kode Containment" id="kode_containment" name="kode_containment" type="text">
                            <label for="kode_containment">Kode Containment</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Nama Containment" id="nama_containment" name="nama_containment" type="text">
                            <label for="nama_containment">Nama Containment</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea placeholder="Keterangan" id="keterangan" name="keterangan" class="materialize-textarea"></textarea>
                            <label for="keterangan" class="">Keterangan</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <button class="btn gradient-45deg-purple-deep-orange waves-effect waves-light right" type="button" id="btn_simpan">Simpan
                                <i class="material-icons right">send</i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@push('script-footer')
        <script src="{{url('assets/vendors/select2/select2.full.min.js')}}"></script>
        <script src="{{url('assets/vendors/select2/form-select2.min.js')}}"></script>
        <script src="{{url('js/master/mscontainment/add_app.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('.atastombol').removeClass('active bold open');
                $('.tombol').removeClass('waves-effect waves-cyan active');
                $('#idBtnMaster').addClass('active bold open');
                $("#bodyMaster").css("display", "block");
                $('#btnMasterContainment').addClass('waves-effect waves-cyan active');
            });
        </script>

        <script type="text/javascript">
            var url_api = "{{url('api/v1/master/containment/store')}}"
        </script>


        <script type="text/javascript">
            if ($('#kode_pusdakim').on('change', function(){
                $('#kode_ruangan').removeAttr("disabled");
                $.ajax({
                    type: "get",
                    url: '{{url("master/containment/pullData")}}',
                    data: {
                        type            : 'pullData',
                        kode_pusdakim     : $('#kode_pusdakim').val()
                    },

                    success: function(data) {
                        $('#kode_ruangan').empty();
                        $('option:selected', '#kode_ruangan').remove();
                        $('#kode_ruangan').append($("<option></option>").text("Pilih Ruangan").val(''));
                        $.each(data, function(i, item) {
                            $('#kode_ruangan').append($('<option></option>').val(item.kode_ruangan).html(item.display).data('nama_ruangan', item.nama_ruangan));
                            $('#kode_ruangan').select2();          
                        });
                    }
                });
            }));
        </script>
@endpush
@endsection


