<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/master', 'middleware' => 'auth:api'], function () {

	Route::group(['prefix' => 'pegawai'], function () {
		Route::POST('store', 'PegawaiController@store');
        Route::POST('edit', 'PegawaiController@edit');
        Route::POST('editshow', 'PegawaiController@editshow');
        Route::POST('nonaktif/{id}', 'PegawaiController@nonaktif');
        Route::POST('ubahPassword', 'PegawaiController@ubahPassword');
        Route::POST('ubahPasswordAdmin', 'PegawaiController@ubahPasswordAdmin')->middleware('permission:Mengubah data pegawai lain');
    });

    Route::group(['prefix' => 'subdirektorat'], function () {
		Route::POST('store', 'MsSubdirektoratController@store');
        Route::POST('edit', 'MsSubdirektoratController@edit');
        Route::POST('delete/{id}', 'MsSubdirektoratController@delete');
        Route::POST('deletepermanent/{id}', 'MsSubdirektoratController@deletepermanent');
    });

    Route::group(['prefix' => 'seksi'], function () {
        Route::POST('store', 'MsSeksiController@store');
        Route::POST('edit', 'MsSeksiController@edit');
        Route::POST('delete/{id}', 'MsSeksiController@delete');
        Route::POST('deletepermanent/{id}', 'MsSeksiController@deletepermanent');
    });

    Route::group(['prefix' => 'jeniskegiatan'], function () {
        Route::POST('store', 'MsJenisKegiatanController@store');
        Route::POST('edit', 'MsJenisKegiatanController@edit');
        Route::POST('delete/{id}', 'MsJenisKegiatanController@delete');
        Route::POST('deletepermanent/{id}', 'MsJenisKegiatanController@deletepermanent');
    });

    Route::group(['prefix' => 'pusdakim'], function () {
        Route::POST('store', 'MsPusdakimController@store');
        Route::POST('edit', 'MsPusdakimController@edit');
        Route::POST('delete/{id}', 'MsPusdakimController@delete');
        Route::POST('deletepermanent/{id}', 'MsPusdakimController@deletepermanent');
    });

    Route::group(['prefix' => 'ruangan'], function () {
        Route::POST('store', 'MsRuanganController@store');
        Route::POST('edit', 'MsRuanganController@edit');
        Route::POST('delete/{id}', 'MsRuanganController@delete');
        Route::POST('deletepermanent/{id}', 'MsRuanganController@deletepermanent');
    });

    Route::group(['prefix' => 'containment'], function () {
        Route::POST('store', 'MsContainmentController@store');
        Route::POST('edit', 'MsContainmentController@edit');
        Route::POST('delete/{id}', 'MsContainmentController@delete');
        Route::POST('deletepermanent/{id}', 'MsContainmentController@deletepermanent');
    });

});