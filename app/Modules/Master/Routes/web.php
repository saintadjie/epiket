<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'master', 'middleware' => 'auth'], function () {
    
    Route::group(['prefix' => 'pegawai'], function () {

    	Route::GET('/', 'PegawaiController@index')->middleware('permission:Melihat daftar pegawai');
    	Route::GET('add', 'PegawaiController@page_add')->middleware('permission:Menambah data pegawai');
        Route::GET('profile', 'PegawaiController@page_profile')->middleware('permission:Mengubah data pegawai')->name('profile');
        Route::GET('show/{id}', 'PegawaiController@page_show')->middleware('permission:Mengubah data pegawai lain');
        Route::GET('aktif/{id}', 'PegawaiController@aktif')->middleware('permission:Mengembalikan data pegawai terhapus');

    });

    Route::group(['prefix' => 'subdirektorat'], function () {

    	Route::GET('/', 'MsSubdirektoratController@index')->middleware('permission:Melihat daftar mssubdirektorat');
    	Route::GET('add', 'MsSubdirektoratController@page_add')->middleware('permission:Menambah data mssubdirektorat');
        Route::GET('show/{id}', 'MsSubdirektoratController@page_show')->middleware('permission:Mengubah data mssubdirektorat');
        Route::GET('trash', 'MsSubdirektoratController@trash')->middleware('permission:Melihat data mssubdirektorat terhapus');
        Route::GET('restore/{id}', 'MsSubdirektoratController@restore')->middleware('permission:Mengembalikan data mssubdirektorat terhapus');

    });

    Route::group(['prefix' => 'seksi'], function () {

        Route::GET('/', 'MsSeksiController@index')->middleware('permission:Melihat daftar msseksi');
        Route::GET('add', 'MsSeksiController@page_add')->middleware('permission:Menambah data msseksi');
        Route::GET('show/{id}', 'MsSeksiController@page_show')->middleware('permission:Mengubah data msseksi');
        Route::GET('trash', 'MsSeksiController@trash')->middleware('permission:Melihat data msseksi terhapus');
        Route::GET('restore/{id}', 'MsSeksiController@restore')->middleware('permission:Mengembalikan data msseksi terhapus');

    });

    Route::group(['prefix' => 'jeniskegiatan'], function () {

        Route::GET('/', 'MsJenisKegiatanController@index')->middleware('permission:Melihat daftar msjeniskegiatan');
        Route::GET('add', 'MsJenisKegiatanController@page_add')->middleware('permission:Menambah data msjeniskegiatan');
        Route::GET('show/{id}', 'MsJenisKegiatanController@page_show')->middleware('permission:Mengubah data msjeniskegiatan');
        Route::GET('trash', 'MsJenisKegiatanController@trash')->middleware('permission:Melihat data msjeniskegiatan terhapus');
        Route::GET('restore/{id}', 'MsJenisKegiatanController@restore')->middleware('permission:Mengembalikan data msjeniskegiatan terhapus');

    });

    Route::group(['prefix' => 'pusdakim'], function () {

        Route::GET('/', 'MsPusdakimController@index')->middleware('permission:Melihat daftar mspusdakim');
        Route::GET('add', 'MsPusdakimController@page_add')->middleware('permission:Menambah data mspusdakim');
        Route::GET('show/{id}', 'MsPusdakimController@page_show')->middleware('permission:Mengubah data mspusdakim');
        Route::GET('trash', 'MsPusdakimController@trash')->middleware('permission:Melihat data mspusdakim terhapus');
        Route::GET('restore/{id}', 'MsPusdakimController@restore')->middleware('permission:Mengembalikan data mspusdakim terhapus');

    });

    Route::group(['prefix' => 'ruangan'], function () {

        Route::GET('/', 'MsRuanganController@index')->middleware('permission:Melihat daftar msruangan');
        Route::GET('add', 'MsRuanganController@page_add')->middleware('permission:Menambah data msruangan');
        Route::GET('show/{id}', 'MsRuanganController@page_show')->middleware('permission:Mengubah data msruangan');
        Route::GET('trash', 'MsRuanganController@trash')->middleware('permission:Melihat data msruangan terhapus');
        Route::GET('restore/{id}', 'MsRuanganController@restore')->middleware('permission:Mengembalikan data msruangan terhapus');

    });

    Route::group(['prefix' => 'containment'], function () {

        Route::GET('/', 'MsContainmentController@index')->middleware('permission:Melihat daftar mscontainment');
        Route::GET('add', 'MsContainmentController@page_add')->middleware('permission:Menambah data mscontainment');
        Route::GET('show/{id}', 'MsContainmentController@page_show')->middleware('permission:Mengubah data mscontainment');
        Route::GET('trash', 'MsContainmentController@trash')->middleware('permission:Melihat data mscontainment terhapus');
        Route::GET('restore/{id}', 'MsContainmentController@restore')->middleware('permission:Mengembalikan data mscontainment terhapus');
        Route::get('pullData', 'MsContainmentController@pullData');

    });
});

