<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsJenisKegiatan extends Model
{
    use SoftDeletes;
    protected $table = 'ms_jeniskegiatan';

    protected $fillable = [
    	'kode_jeniskegiatan',
    	'nama_jeniskegiatan',
    ];
}
