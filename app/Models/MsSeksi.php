<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsSeksi extends Model
{
	use SoftDeletes;
    protected $table = 'ms_seksi';

    protected $fillable = [
    	'kode_seksi',
    	'nama_seksi',
    	'kode_subdirektorat',
    ];
}
