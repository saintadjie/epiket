<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsRuangan extends Model
{
	use SoftDeletes;
    protected $table = 'ms_ruangan';

    protected $fillable = [
    	'kode_ruangan',
    	'nama_ruangan',
    	'keterangan',
    	'kode_pusdakim',
    ];
}
