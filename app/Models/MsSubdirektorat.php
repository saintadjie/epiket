<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsSubdirektorat extends Model
{
	use SoftDeletes;
    protected $table = 'ms_subdirektorat';

    protected $fillable = [
    	'kode_subdirektorat',
    	'nama_subdirektorat',
    ];
}
