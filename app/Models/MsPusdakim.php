<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsPusdakim extends Model
{
    use SoftDeletes;
    protected $table = 'ms_pusdakim';

    protected $fillable = [
    	'kode_pusdakim',
    	'nama_pusdakim',
    	'lokasi_pusdakim',
    	'keterangan',
    ];
}
