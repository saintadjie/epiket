<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsRak extends Model
{
	use SoftDeletes;
    protected $table = 'ms_rak';

    protected $fillable = [
    	'kode_rak',
    	'nama_rak',
    	'keterangan',
    	'kode_containment',
    ];
}
