<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model
{
    use SoftDeletes;
	
    protected $table = "users";
    
    protected $fillable = ['nip', 'nama', 'password', 'kontak', 'kode_subdirektorat', 'kode_seksi', 'level_pengguna', 'photo', 'deleted_at'];

    protected $dates = ['deleted_at'];
}
