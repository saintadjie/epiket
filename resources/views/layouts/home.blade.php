<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-PIKET</title>
    <meta name="description" content="App Monitoring Piket">
    <meta content="Imigrasi" name="author" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{url('assets/images/logosm.png')}}"/>

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- BEGIN: VENDOR CSS-->

    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/css/vendors.min.css')}}">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/css/materialize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/css/style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/css/icon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/vendors/sweetalert2/sweetalert2.min.css')}}" />
    <!-- END: Page Level CSS-->
    @stack('script-header')
</head>
<!-- END: Head-->
<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark preload-transitions 2-columns   " data-open="click" data-menu="vertical-menu-nav-dark" data-col="2-columns">

    <!-- BEGIN: Header-->
    <header class="page-topbar" id="header">
      	<div class="navbar navbar-fixed"> 
        	<nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-purple-deep-orange gradient-shadow">
                <div class="nav-wrapper">
                    <ul class="navbar-list right">
                        <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
                        <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online">
                            @if(Auth::user()->photo != null)
                            <img class="rounded-circle" src="{{url(Auth::user()->photo)}}" alt="User">
                            @else
                                <img class="rounded-circle" src="{{url('assets\images\user.png')}}" alt="User">
                            @endif
                            <i></i></span></a>
                        </li>
                    </ul>
          
                    <!-- profile-dropdown-->
                    <ul class="dropdown-content" id="profile-dropdown">
                        <li><a class="grey-text text-darken-1" href="user-profile-page.html"><i class="material-icons">person_outline</i> Profile</a></li>
                        <li><a class="grey-text text-darken-1" href="{{ route('logout') }}"><i class="material-icons">keyboard_tab</i> Logout</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- END: Header-->

    <!-- BEGIN: SideNav-->
    <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light navbar-full sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img src="{{url('assets/images/logosm.png')}}" alt="E-PIKET"><span class="logo-text hide-on-med-and-down">E-PIKET</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
            <li id="idBtnDashboard" class="atastombol"><a id="aBtnDashboard" class="tombol" href="{{ route('home')}}"><i class="material-icons">settings_input_svideo</i><span data-i18n="Icons">Dashboard</span></a></li>

            <li class="navigation-header"><a class="navigation-header-text">Menu </a><i class="navigation-header-icon material-icons">more_horiz</i></li>

            <li id="idBtnMaster" class="atastombol"><a id="aBtnMaster" class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">apps</i><span class="menu-title" data-i18n="Pages">Master</span></a>
                <div class="collapsible-body btnBody" id="bodyMaster">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        @can('Melihat daftar pegawai')
                        <li><a id="btnMasterPegawai" class="tombol" href="{{url('master/pegawai')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Pegawai</span></a></li>
                        @endcan
                        @can('Melihat daftar mssubdirektorat')
                        <li><a id="btnMasterSubdirektorat" class="tombol" href="{{url('master/subdirektorat')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Blog">Master Subdirektorat</span></a></li>
                        @endcan
                        @can('Melihat daftar msseksi')
                        <li><a id="btnMasterSeksi" class="tombol" href="{{url('master/seksi')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Seksi</span></a></li>
                        @endcan
                        @can('Melihat daftar msjeniskegiatan')
                        <li><a id="btnMasterJenisKegiatan" class="tombol" href="{{url('master/jeniskegiatan')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Jenis Kegiatan</span></a></li>
                        @endcan
                        @can('Melihat daftar mspusdakim')
                        <li><a id="btnMasterPusdakim" class="tombol" href="{{url('master/pusdakim')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Pusdakim</span></a></li>
                        @endcan
                        @can('Melihat daftar msruangan')
                        <li><a id="btnMasterRuangan" class="tombol" href="{{url('master/ruangan')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Ruangan</span></a></li>
                        @endcan
                        @can('Melihat daftar mscontainment')
                        <li><a id="btnMasterContainment" class="tombol" href="{{url('master/containment')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Containment</span></a></li>
                        @endcan
                        @can('Melihat daftar msrack')
                        <li><a id="btnMasterRack" class="tombol" href="{{url('master/rack')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Search">Master Rack</span></a></li>
                        @endcan
                    </ul>
                </div>
            </li>

            <li id="idBtnAktifitas" class="atastombol"><a id="aBtnAktifitas" class="collapsible-header waves-effect waves-cyan " href="JavaScript:void(0)"><i class="material-icons">apps</i><span class="menu-title" data-i18n="Pages">Aktifitas</span></a>
                <div class="collapsible-body btnBody">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                        <li><a id="" class="tombol" href="{{url('master/pegawai')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Contact">Pendataan Tamu/Vendor</span></a></li>
                        <li><a id="" class="tombol" href="{{url('master/pegawai')}}"><i class="material-icons">radio_button_unchecked</i><span data-i18n="Blog">Master Subdirektorat</span></a></li>
                    </ul>
                </div>
            </li>

        </ul>
        <div class="navigation-background"></div>
        <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        <div class="row">
            @yield('content')
        </div>
    </div>
    <!-- END: Page Main-->


    <!-- Theme Customizer -->
    <a href="#" data-target="theme-cutomizer-out" class="btn btn-customizer pink accent-2 white-text sidenav-trigger theme-cutomizer-trigger"><i class="material-icons">settings</i></a>

    <div id="theme-cutomizer-out" class="theme-cutomizer sidenav row">
        <div class="col s12">
            <a class="sidenav-close" href="#!"><i class="material-icons">close</i></a>
            <h5 class="theme-cutomizer-title">Ubah Tema</h5>
            <p class="medium-small">Ubah dan coba secara real time</p>
            <div class="menu-options">
                <h6 class="mt-6">Menu Options</h6>
                <hr class="customize-devider" />
                <div class="menu-options-form row">
                    <div class="input-field col s12 menu-color mb-0">
                        <p class="mt-0">Menu Color</p>
                        <div class="gradient-color center-align">
                            <span class="menu-color-option gradient-45deg-indigo-blue" data-color="gradient-45deg-indigo-blue"></span>
                            <span class="menu-color-option gradient-45deg-purple-deep-orange" data-color="gradient-45deg-purple-deep-orange"></span>
                            <span class="menu-color-option gradient-45deg-light-blue-cyan" data-color="gradient-45deg-light-blue-cyan"></span>
                            <span class="menu-color-option gradient-45deg-purple-amber" data-color="gradient-45deg-purple-amber"></span>
                            <span class="menu-color-option gradient-45deg-purple-deep-purple" data-color="gradient-45deg-purple-deep-purple"></span>
                            <span class="menu-color-option gradient-45deg-deep-orange-orange" data-color="gradient-45deg-deep-orange-orange"></span>
                            <span class="menu-color-option gradient-45deg-green-teal" data-color="gradient-45deg-green-teal"></span>
                            <span class="menu-color-option gradient-45deg-indigo-light-blue" data-color="gradient-45deg-indigo-light-blue"></span>
                            <span class="menu-color-option gradient-45deg-red-pink" data-color="gradient-45deg-red-pink"></span>
                        </div>
                        <div class="solid-color center-align">
                            <span class="menu-color-option red" data-color="red"></span>
                            <span class="menu-color-option purple" data-color="purple"></span>
                            <span class="menu-color-option pink" data-color="pink"></span>
                            <span class="menu-color-option deep-purple" data-color="deep-purple"></span>
                            <span class="menu-color-option cyan" data-color="cyan"></span>
                            <span class="menu-color-option teal" data-color="teal"></span>
                            <span class="menu-color-option light-blue" data-color="light-blue"></span>
                            <span class="menu-color-option amber darken-3" data-color="amber darken-3"></span>
                            <span class="menu-color-option brown darken-2" data-color="brown darken-2"></span>
                        </div>
                    </div>

                    <div class="input-field col s12 menu-bg-color mb-0">
                        <p class="mt-0">Menu Background Color</p>
                        <div class="gradient-color center-align">
                            <span class="menu-bg-color-option gradient-45deg-indigo-blue" data-color="gradient-45deg-indigo-blue"></span>
                            <span class="menu-bg-color-option gradient-45deg-purple-deep-orange" data-color="gradient-45deg-purple-deep-orange"></span>
                            <span class="menu-bg-color-option gradient-45deg-light-blue-cyan" data-color="gradient-45deg-light-blue-cyan"></span>
                            <span class="menu-bg-color-option gradient-45deg-purple-amber" data-color="gradient-45deg-purple-amber"></span>
                            <span class="menu-bg-color-option gradient-45deg-purple-deep-purple" data-color="gradient-45deg-purple-deep-purple"></span>
                            <span class="menu-bg-color-option gradient-45deg-deep-orange-orange" data-color="gradient-45deg-deep-orange-orange"></span>
                            <span class="menu-bg-color-option gradient-45deg-green-teal" data-color="gradient-45deg-green-teal"></span>
                            <span class="menu-bg-color-option gradient-45deg-indigo-light-blue" data-color="gradient-45deg-indigo-light-blue"></span>
                            <span class="menu-bg-color-option gradient-45deg-red-pink" data-color="gradient-45deg-red-pink"></span>
                        </div>
                        <div class="solid-color center-align">
                            <span class="menu-bg-color-option red" data-color="red"></span>
                            <span class="menu-bg-color-option purple" data-color="purple"></span>
                            <span class="menu-bg-color-option pink" data-color="pink"></span>
                            <span class="menu-bg-color-option deep-purple" data-color="deep-purple"></span>
                            <span class="menu-bg-color-option cyan" data-color="cyan"></span>
                            <span class="menu-bg-color-option teal" data-color="teal"></span>
                            <span class="menu-bg-color-option light-blue" data-color="light-blue"></span>
                            <span class="menu-bg-color-option amber darken-3" data-color="amber darken-3"></span>
                            <span class="menu-bg-color-option brown darken-2" data-color="brown darken-2"></span>
                        </div>
                    </div>

                    <div class="input-field col s12">
                        <div class="switch">
                            Menu Dark
                            <label class="float-right"><input class="menu-dark-checkbox" type="checkbox"/> <span class="lever ml-0"></span></label>
                        </div>
                    </div>
      
                    <div class="input-field col s12">
                        <div class="switch">
                            Menu Collapsed
                            <label class="float-right"><input class="menu-collapsed-checkbox" type="checkbox"/> <span class="lever ml-0"></span></label>
                        </div>
                    </div>

                    <div class="input-field col s12">
                        <div class="switch">
                            <p class="mt-0">Menu Selection</p>
                            <label>
                                <input class="menu-selection-radio with-gap" value="sidenav-active-square" name="menu-selection" type="radio"/>
                                <span>Square</span>
                            </label>
                            <label>
                                <input class="menu-selection-radio with-gap" value="sidenav-active-rounded" name="menu-selection" type="radio"/>
                                <span>Rounded</span>
                            </label>
                            <label>
                                <input class="menu-selection-radio with-gap" value="" name="menu-selection" type="radio" />
                                <span>Normal</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <h6 class="mt-6">Navbar Options</h6>
            <hr class="customize-devider" />
            <div class="navbar-options row">
                <div class="input-field col s12 navbar-color mb-0">
                    <p class="mt-0">Navbar Color</p>
                    <div class="gradient-color center-align">
                        <span class="navbar-color-option gradient-45deg-indigo-blue" data-color="gradient-45deg-indigo-blue"></span>
                        <span class="navbar-color-option gradient-45deg-purple-deep-orange" data-color="gradient-45deg-purple-deep-orange"></span>
                        <span class="navbar-color-option gradient-45deg-light-blue-cyan" data-color="gradient-45deg-light-blue-cyan"></span>
                        <span class="navbar-color-option gradient-45deg-purple-amber" data-color="gradient-45deg-purple-amber"></span>
                        <span class="navbar-color-option gradient-45deg-purple-deep-purple" data-color="gradient-45deg-purple-deep-purple"></span>
                        <span class="navbar-color-option gradient-45deg-deep-orange-orange"data-color="gradient-45deg-deep-orange-orange"></span>
                        <span class="navbar-color-option gradient-45deg-green-teal" data-color="gradient-45deg-green-teal"></span>
                        <span class="navbar-color-option gradient-45deg-indigo-light-blue" data-color="gradient-45deg-indigo-light-blue"></span>
                        <span class="navbar-color-option gradient-45deg-red-pink" data-color="gradient-45deg-red-pink"></span>
                    </div>
                    <div class="solid-color center-align">
                        <span class="navbar-color-option red" data-color="red"></span>
                        <span class="navbar-color-option purple" data-color="purple"></span>
                        <span class="navbar-color-option pink" data-color="pink"></span>
                        <span class="navbar-color-option deep-purple" data-color="deep-purple"></span>
                        <span class="navbar-color-option cyan" data-color="cyan"></span>
                        <span class="navbar-color-option teal" data-color="teal"></span>
                        <span class="navbar-color-option light-blue" data-color="light-blue"></span>
                        <span class="navbar-color-option amber darken-3" data-color="amber darken-3"></span>
                        <span class="navbar-color-option brown darken-2" data-color="brown darken-2"></span>
                    </div>
                </div>

                <div class="input-field col s12">
                    <div class="switch">
                        Navbar Dark
                        <label class="float-right"><input class="navbar-dark-checkbox" type="checkbox"/> <span class="lever ml-0"></span></label>
                    </div>
                </div>

                <div class="input-field col s12">
                    <div class="switch">
                        Navbar Fixed
                        <label class="float-right"><input class="navbar-fixed-checkbox" type="checkbox" checked/> <span class="lever ml-0"></span></label>
                    </div>
                </div>
            </div>

            <h6 class="mt-6">Footer Options</h6>
            <hr class="customize-devider" />
            <div class="navbar-options row">
                <div class="input-field col s12">
                    <div class="switch">
                        Footer Dark
                        <label class="float-right"><input class="footer-dark-checkbox" type="checkbox"/> <span class="lever ml-0"></span></label>
                    </div>
                </div>
                <div class="input-field col s12">
                    <div class="switch">
                        Footer Fixed
                        <label class="float-right"><input class="footer-fixed-checkbox" type="checkbox"/> <span class="lever ml-0"></span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ Theme Customizer -->

    <!-- BEGIN: Footer-->
    <footer class="page-footer footer footer-static footer-dark gradient-45deg-purple-deep-orange gradient-shadow navbar-border navbar-shadow">
        <div class="footer-copyright">
            <div class="container"><span>&copy; 2020          KEMENKUMHAM - DITJEN IMIGRASI.</span><span class="right hide-on-small-only">Design and Developed by DITSISTIK</span></div>
        </div>
    </footer>
    <!-- END: Footer-->

    <!-- BEGIN VENDOR JS-->
    <script src="{{url('assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->

    <script src="{{url('assets/vendors/js/plugins.min.js')}}"></script>
    <script src="{{url('assets/vendors/js/search.min.js')}}"></script>
    <script src="{{url('assets/vendors/js/customizer.min.js')}}"></script>
    <script src="{{url('assets/vendors/sweetalert2/sweetalert2.min.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-Requested-With': 'XMLHttpRequest',
            }
        })
    </script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
</body>

    @stack('script-footer')
</html>