<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>E-PIKET</title>
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="{{url('assets/images/logosm.png')}}"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!-- BEGIN: VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('assets/login/css/vendors.min.css')}}">
        <!-- END: VENDOR CSS-->
        <!-- BEGIN: Page Level CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('assets/login/css/materialize.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('assets/login/css/style.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('assets/login/css/login.min.css')}}">
        <!-- END: Page Level CSS-->
        <!--===============================================================================================-->

    </head>

    <body class="vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark preload-transitions 1-column login-bg   blank-page blank-page" data-open="click" data-menu="vertical-menu-nav-dark" data-col="1-column">   
                                
        @yield('content')

    </body>
    <!-- BEGIN VENDOR JS-->
    <script src="{{url('assets/login/js/vendors.min.js')}}"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="{{url('assets/login/js/plugins.min.js')}}"></script>
    <script src="{{url('assets/login/js/search.min.js')}}"></script>
    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
</html>
