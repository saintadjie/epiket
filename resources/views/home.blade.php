@extends('layouts.home')

@section('content')
<div class="breadcrumbs-inline pt-3 pb-1" id="breadcrumbs-wrapper">
    <div class="container">
        <div class="row">
            
            <div class="col s10 m6 l6 breadcrumbs-left">
                <h5 class="breadcrumbs-title mt-0 mb-0 display-inline hide-on-small-and-down"><span>Icons</span></h5>
                <ol class="breadcrumbs mb-0">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Basic UI</a></li>
                    <li class="breadcrumb-item active">Icons</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="col s12">
    <div class="container">
        <div class="section">

            <!--Icon Sizes-->
            <div class="row">
                <div class="col s12 m12 l12">
                    <div id="icon-sizes" class="card card-default">
                        <div class="card-content">
                            <h4 class="card-title">Usage</h4>
                            <div class="row">
                                <div class="col s12">
                                    <p>To be able to use these icons, you must include this line in the <code class="language-markup">&lt;head></code>portion
                                    of your HTML code</p>
                                    <pre><code class="language-markup">
                                        &lt;link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></code></pre>
                                    <p>To use these icons, use the material-icons class on an element and provide the ligature
                                    as the text content.</p>
                                    <pre><code class="language-markup">
                                        &lt;i class="material-icons">add&lt;/i>
                                    </code></pre>
                                </div>
                            </div>
                            <h4 class="card-title">Sizes</h4>
                            <div class="row">
                                <div class="col s12">
                                    <p>To control the size of the icon, change the <code class=" language-css"><span class="token property">font-size</span><span
                                        class="token punctuation">:</span> 30px</code> property of your icon.
                                    Optionally you can use preset classes as shown below.</p>
                                </div>
                                <div class="col s12">
                                    <div class="card-panel">
                                        <div class="row">
                                            <div class="center-align">
                                                <div class="col s3">
                                                    <i class="material-icons tiny icon-demo">insert_chart</i>
                                                    <br>
                                                    <p>Tiny</p>
                                                </div>
                                                <div class="col s3">
                                                    <i class="material-icons small icon-demo">insert_chart</i>
                                                    <br>
                                                    <p>Small</p>
                                                </div>
                                                <div class="col s3">
                                                    <i class="material-icons medium icon-demo">insert_chart</i>
                                                    <br>
                                                    <p>Medium</p>
                                                </div>
                                                <div class="col s3">
                                                    <i class="material-icons large icon-demo">insert_chart</i>
                                                    <br>
                                                    <p>Large</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Icon Listing -->   
        </div><!-- START RIGHT SIDEBAR NAV -->

        <aside id="right-sidebar-nav">
            <div id="slide-out-right" class="slide-out-right-sidenav sidenav rightside-navigation">
                <div class="row">
                    <div class="slide-out-right-title">
                        <div class="col s12 border-bottom-1 pb-0 pt-1">
                            <div class="row">
                                <div class="col s2 pr-0 center">
                                    <i class="material-icons vertical-text-middle"><a href="#" class="sidenav-close">clear</a></i>
                                </div>
                                <div class="col s10 pl-0">
                                    <ul class="tabs">
                                        <li class="tab col s4 p-0">
                                            <a href="#messages" class="active">
                                                <span>Messages</span>
                                            </a>
                                        </li>
                                        <li class="tab col s4 p-0">
                                            <a href="#settings">
                                                <span>Settings</span>
                                            </a>
                                        </li>
                                        <li class="tab col s4 p-0">
                                            <a href="#activity">
                                                <span>Activity</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- END RIGHT SIDEBAR NAV -->
    </div>
    <div class="content-overlay"></div>
</div>
@push('script-footer')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.atastombol').removeClass('active bold open');
            $('.tombol').removeClass('waves-effect waves-cyan active');
            $('#aBtnDashboard').addClass('waves-effect waves-cyan active');
        });
    </script>
@endpush
@endsection
