$(function () {

    var kode_ruanganlama = "{{$result->kode_ruangan}}";
    
    $('#btn_simpan').click(function(){

        if ($('#kode_pusdakim').val() == null) {
            Swal.fire( "Kesalahan", "Pusdakim harus dipilih", "error" )
            return
        } else if ($('#kode_ruangan').val() == '') {
            Swal.fire( "Kesalahan", "Kode Ruangan tidak boleh kosong", "error" )
            return
        } else if ($('#nama_ruangan').val() == '') {
            Swal.fire( "Kesalahan", "Nama Ruangan tidak boleh kosong", "error" )
            return
        }

        $("input:disabled").removeAttr("disabled")
        
        var uploadfile = new FormData($("#form_ruangan")[0])
        uploadfile.append('id', id)

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Kode Ruangan : "+$('#kode_ruanganlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: url_api,
                    data: uploadfile,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_ruangan
                            });
                        }
                         else if(data.status == 'Failed' && data.message == 'Duplicate') {
                            Swal.fire("Kesalahan", "Ruangan sudah ada", "error")
                        } else if(data.status == 'Failed' && data.message == 'Trash') {
                            Swal.fire("Kesalahan", "Ruangan dengan kode: " + $('#kode_ruangan').val() + " sudah ada namun tidak aktif", "error")
                        }
                    }
                });
            }
        })
        
    })

})