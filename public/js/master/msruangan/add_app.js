$(function () {

    $('#btn_simpan').click(function(){
        if ($('#kode_pusdakim').val() == null) {
            Swal.fire( "Kesalahan", "Pusdakim harus dipilih", "error" )
            return
        } else if ($('#kode_ruangan').val() == '') {
            Swal.fire( "Kesalahan", "Kode Ruangan tidak boleh kosong", "error" )
            return
        } else if ($('#nama_ruangan').val() == '') {
            Swal.fire( "Kesalahan", "Nama Ruangan tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_ruangan")[0])

        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
        }).done(function(data){

            if(data.status == 'OK'){
                Swal.fire({
                    icon: 'success',
                    title: "Data telah disimpan",
                    timer: 3000,
                    showConfirmButton: true,
                    html: 'Otomatis tertutup dalam <b></b> milidetik.',
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                          const content = Swal.getContent()
                          if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                              b.textContent = Swal.getTimerLeft()
                            }
                          }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(function (result) {
                    $('#kode_pusdakim').val('pilih').trigger('change');
                    $('#kode_ruangan').val('')
                    $('#nama_ruangan').val('')
                    $('#keterangan').val('')
                })

            } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                Swal.fire("Kesalahan", "Kode Ruangan sudah ada", "error")
            } else if(data.status == 'Failed' && data.message == 'Trash') {
                Swal.fire("Kesalahan", "Ruangan dengan kode: " + $('#kode_ruangan').val() + " sudah ada namun tidak aktif", "error")
            }

        })
    })

})
