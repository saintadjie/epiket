$(function () {

    var kode_seksilama = "{{$result->kode_seksi}}";
    
    $('#btn_simpan').click(function(){

        if ($('#kode_subdirektorat').val() == null) {
            Swal.fire( "Kesalahan", "Subdirektorat harus dipilih", "error" )
            return
        } else if ($('#kode_seksi').val() == '') {
            Swal.fire( "Kesalahan", "Kode Seksi tidak boleh kosong", "error" )
            return
        } else if ($('#nama_seksi').val() == '') {
            Swal.fire( "Kesalahan", "Nama Seksi tidak boleh kosong", "error" )
            return
        }

        $("input:disabled").removeAttr("disabled")
        
        var uploadfile = new FormData($("#form_seksi")[0])
        uploadfile.append('id', id)

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Kode Seksi : "+$('#kode_seksilama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: url_api,
                    data: uploadfile,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_seksi
                            });
                        }
                         else if(data.status == 'Failed' && data.message == 'Duplicate') {
                            Swal.fire("Kesalahan", "Seksi sudah ada", "error")
                        } else if(data.status == 'Failed' && data.message == 'Trash') {
                            Swal.fire("Kesalahan", "Seksi dengan kode: " + $('#kode_seksi').val() + " sudah ada namun tidak aktif", "error")
                        }
                    }
                });
            }
        })
        
    })

})