$(function () {

    $('#btn_simpan').click(function(){
        if ($('#kode_subdirektorat').val() == null) {
            Swal.fire( "Kesalahan", "Subdirektorat harus dipilih", "error" )
            return
        } else if ($('#kode_seksi').val() == '') {
            Swal.fire( "Kesalahan", "Kode Seksi tidak boleh kosong", "error" )
            return
        } else if ($('#nama_seksi').val() == '') {
            Swal.fire( "Kesalahan", "Nama Seksi tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_seksi")[0])

        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
        }).done(function(data){

            if(data.status == 'OK'){
                Swal.fire({
                    icon: 'success',
                    title: "Data telah disimpan",
                    timer: 3000,
                    showConfirmButton: true,
                    html: 'Otomatis tertutup dalam <b></b> milidetik.',
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                          const content = Swal.getContent()
                          if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                              b.textContent = Swal.getTimerLeft()
                            }
                          }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(function (result) {
                    $('#kode_subdirektorat').val('pilih').trigger('change');
                    $('#kode_seksi').val('')
                    $('#nama_seksi').val('')
                })

            } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                Swal.fire("Kesalahan", "Kode Seksi sudah ada", "error")
            } else if(data.status == 'Failed' && data.message == 'Trash') {
                Swal.fire("Kesalahan", "Seksi dengan kode: " + $('#kode_seksi').val() + " sudah ada namun tidak aktif", "error")
            }

        })
    })

})
