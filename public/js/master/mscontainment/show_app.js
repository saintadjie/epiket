$(function () {

    var kode_containmentlama = "{{$result->kode_containment}}";
    
    $('#btn_simpan').click(function(){

        if ($('#kode_pusdakim').val() == null || $('#kode_pusdakim').val() == '') {
            Swal.fire( "Kesalahan", "Pusdakim harus dipilih", "error" )
            return
        } else if ($('#kode_ruangan').val() == null || $('#kode_ruangan').val() == '') {
            Swal.fire( "Kesalahan", "Ruangan harus dipilih", "error" )
            return
        } else if ($('#kode_containment').val() == '') {
            Swal.fire( "Kesalahan", "Kode Containment tidak boleh kosong", "error" )
            return
        } else if ($('#nama_containment').val() == '') {
            Swal.fire( "Kesalahan", "Nama Containment tidak boleh kosong", "error" )
            return
        }

        $("select:disabled").removeAttr("disabled")
        
        var uploadfile = new FormData($("#form_containment")[0])
        uploadfile.append('id', id)

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Kode Containment : "+$('#kode_containmentlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: url_api,
                    data: uploadfile,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_containment
                            });
                        }
                         else if(data.status == 'Failed' && data.message == 'Duplicate') {
                            Swal.fire("Kesalahan", "Containment sudah ada", "error")
                        } else if(data.status == 'Failed' && data.message == 'Trash') {
                            Swal.fire("Kesalahan", "Containment dengan kode: " + $('#kode_containment').val() + " sudah ada namun tidak aktif", "error")
                        }
                    }
                });
            }
        })
        
    })

})