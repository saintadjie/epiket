$(function () {

    $('#page-length-option tbody').on('click', '#btn_hapus', function(){

        var id = $(this).closest('tr').attr('id')
        var kodecontainment = $(this).data('kodecontainment')


        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Containment dengan kode: "+kodecontainment+" akan dihapus?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_delete + '/' + id,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah dihapus",
                                timer: 3000,
                                showConfirmButton: true,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_mscontainment
                            });
                        }
                        else if(data.status=='ERROR'){
                            Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
                        }
                    }
                });
            }
        })

    })

})

