$(function () {

    $('#btn_simpan').click(function(){
        if ($('#kode_jeniskegiatan').val() == '') {
            Swal.fire( "Kesalahan", "Kode Jenis Kegiatan tidak boleh kosong", "error" )
            return
        } else if ($('#nama_jeniskegiatan').val() == '') {
            Swal.fire( "Kesalahan", "Nama Jenis Kegiatan tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_jeniskegiatan")[0])

        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
        }).done(function(data){

            if(data.status == 'OK'){
                Swal.fire({
                    icon: 'success',
                    title: "Data telah disimpan",
                    timer: 3000,
                    showConfirmButton: true,
                    html: 'Otomatis tertutup dalam <b></b> milidetik.',
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                          const content = Swal.getContent()
                          if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                              b.textContent = Swal.getTimerLeft()
                            }
                          }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(function (result) {
                    $('#kode_jeniskegiatan').val('')
                    $('#nama_jeniskegiatan').val('')
                })

            } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                Swal.fire("Kesalahan", "Kode Jenis Kegiatan sudah ada", "error")
            } else if(data.status == 'Failed' && data.message == 'Trash') {
                Swal.fire("Kesalahan", "Jenis Kegiatan dengan kode: " + $('#kode_jeniskegiatan').val() + " sudah ada namun tidak aktif", "error")
            }

        })
    })

})
