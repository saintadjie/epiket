$(function () {

    $('#page-length-option tbody').on('click', '#btn_restore', function(){

        var id = $(this).closest('tr').attr('id')
        var kodejeniskegiatan = $(this).data('kodejeniskegiatan')


        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Jenis Kegiatan dengan kode: "+kodejeniskegiatan+" akan dikembalikan?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"

        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: url_restore + '/' + id,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah dikembalikan",
                                timer: 3000,
                                showConfirmButton: true,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_trash
                            });
                        }
                        else if(data.status=='ERROR'){
                            Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
                        }
                    }
                });
            }
        })

    })

    $('#page-length-option tbody').on('click', '#btn_hapus', function(){

        var id = $(this).closest('tr').attr('id')
        var kodejeniskegiatan = $(this).data('kodejeniskegiatan')


        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Jenis Kegiatan dengan kode: "+kodejeniskegiatan+" akan dihapus permanen?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"

        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_deletepermanent + '/' + id,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah dihapus permanen",
                                timer: 3000,
                                showConfirmButton: true,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_trash
                            });
                        }
                        else if(data.status=='ERROR'){
                            Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
                        }
                    }
                });
            }
        })

    })

})