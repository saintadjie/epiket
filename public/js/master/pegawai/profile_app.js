$(function () {

	var niplama = "{{$result->nip}}";

    $('#photo').fileinput({
        showUpload: false,
        showRemove: false,
        maxFileSize : 500,
        allowedFileExtensions: ['jpg', 'png', 'jpeg'],

        fileActionSettings: { showZoom: false }
    })
    
	$('#btn_simpan').click(function(){

        if ($('#nip').val() == '') {
            Swal.fire( "Kesalahan", "NIP tidak boleh kosong", "error" )
            return
        } else if ($('#nip').val().length != 18) {
            Swal.fire( "Kesalahan", "Silahkan Masukkan NIP 18 Digit", "error" )
            return
        } else if ($('#nama').val() == '') {
            Swal.fire( "Kesalahan", "Nama tidak boleh kosong", "error" )
            return
        } else if ($('#level_pengguna')[0].selectedIndex <= 0) {
            Swal.fire( "Kesalahan", "Silahkan pilih Hak Akses terlebih dahulu", "error" )
            return
        } else if ($('#kode_subdirektorat')[0].selectedIndex <= 0) {
            Swal.fire( "Kesalahan", "Silahkan pilih Direktorat terlebih dahulu", "error" )
            return
        }
        
        $("input:disabled").removeAttr("disabled")
        var uploadfile = new FormData($("#form_pegawai")[0])
        uploadfile.append('id', id)


        Swal.fire({
            title: "Apakah anda yakin?",
            text: "NIP Pegawai : "+$('#niplama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: url_api,
                    data: uploadfile,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_profile
                            });
                        }
                         else if(data.status == 'Failed' && data.message == 'Duplicate') {
                            Swal.fire("Kesalahan", "Pengguna sudah ada", "error")
                        } else if(data.status == 'Failed' && data.message == 'Trash') {
                            Swal.fire("Kesalahan", "Pengguna dengan NIP: " + $('#nip').val() + " sudah ada namun tidak aktif", "error")
                        }
                    }
                });
            }
        })
        
    })

})