$(function () {

    $('#tb_pegawai tbody').on('click', '#btn_nonaktif', function(){

        var id = $(this).closest('tr').attr('id')
        var nip = $(this).data('kodenip')


        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Pengguna dengan NIP: "+nip+" akan dinonaktifkan?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_nonaktif + '/' + id,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah dinonaktifkan",
                                timer: 3000,
                                showConfirmButton: true,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_pegawai
                            });
                        }
                        else if(data.status=='ERROR'){
                            Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
                        }
                    }
                });
            }
        })

    })

    $('#tb_pegawai tbody').on('click', '#btn_aktif', function(){

        var id = $(this).closest('tr').attr('id')
        var nip = $(this).data('kodenip')


        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Pengguna dengan NIP: "+nip+" akan diaktifkan?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "GET",
                    url: url_aktif + '/' + id,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diaktifkan",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_pegawai
                            });
                        }
                        else if(data.status=='ERROR'){
                            Swal.fire("Kesalahan", "Permintaan tidak dapat diproses", "error");
                        }
                    }
                });
            }
        })

    })

})

