$(function () {

    $('#btn_simpan').click(function(){
        if ($('#kode_subdirektorat').val() == '') {
            Swal.fire( "Kesalahan", "Kode Subdirektorat tidak boleh kosong", "error" )
            return
        } else if ($('#nama_subdirektorat').val() == '') {
            Swal.fire( "Kesalahan", "Nama Subdirektorat tidak boleh kosong", "error" )
            return
        }

        var uploadfile = new FormData($("#form_subdirektorat")[0])

        $.ajax({
            type: "POST",
            url: url_api,
            data: uploadfile,
            processData: false,
            contentType: false,
        }).done(function(data){

            if(data.status == 'OK'){
                Swal.fire({
                    icon: 'success',
                    title: "Data telah disimpan",
                    timer: 3000,
                    showConfirmButton: true,
                    html: 'Otomatis tertutup dalam <b></b> milidetik.',
                    timerProgressBar: true,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                        timerInterval = setInterval(() => {
                          const content = Swal.getContent()
                          if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                              b.textContent = Swal.getTimerLeft()
                            }
                          }
                        }, 100)
                    },
                    onClose: () => {
                        clearInterval(timerInterval)
                    }
                }).then(function (result) {
                    $('#kode_subdirektorat').val('')
                    $('#nama_subdirektorat').val('')
                })

            } else if(data.status == 'Failed' && data.message == 'Duplicate') {
                Swal.fire("Kesalahan", "Kode Subdirektorat sudah ada", "error")
            } else if(data.status == 'Failed' && data.message == 'Trash') {
                Swal.fire("Kesalahan", "Subdirektorat dengan kode: " + $('#kode_subdirektorat').val() + " sudah ada namun tidak aktif", "error")
            }

        })
    })

})
