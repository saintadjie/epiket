$(function () {

	var kode_subdirektoratlama = "{{$result->kode_subdirektorat}}";
    
	$('#btn_simpan').click(function(){

        if ($('#kode_subdirektorat').val() == '') {
            Swal.fire( "Kesalahan", "Kode Subdirektorat tidak boleh kosong", "error" )
            return
        } else if ($('#nama_subdirektorat').val() == '') {
            Swal.fire( "Kesalahan", "Nama Subdirektorat tidak boleh kosong", "error" )
            return
        }

        $("input:disabled").removeAttr("disabled")
        
        var uploadfile = new FormData($("#form_subdirektorat")[0])
        uploadfile.append('id', id)

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Kode Subdirektorat : "+$('#kode_subdirektoratlama').val()+" akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: url_api,
                    data: uploadfile,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.href = url_subdirektorat
                            });
                        }
                         else if(data.status == 'Failed' && data.message == 'Duplicate') {
                            Swal.fire("Kesalahan", "Subdirektorat sudah ada", "error")
                        } else if(data.status == 'Failed' && data.message == 'Trash') {
                            Swal.fire("Kesalahan", "Subdirektorat dengan kode: " + $('#kode_subdirektorat').val() + " sudah ada namun tidak aktif", "error")
                        }
                    }
                });
            }
        })
        
    })

})