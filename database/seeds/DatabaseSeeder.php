<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(RolesandPermissionSeeder::class);
        $this->call(MsSubdirektoratSeeder::class);
        $this->call(MsSeksiSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MsPusdakimSeeder::class);
        $this->call(MsRuanganSeeder::class);
        $this->call(MsContainmentSeeder::class);
        $this->call(MsRakSeeder::class);
    }
}
