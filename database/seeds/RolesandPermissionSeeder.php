<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::create(['name' => 'Melihat daftar pegawai']);
        Permission::create(['name' => 'Menambah data pegawai']);
        Permission::create(['name' => 'Mengubah data pegawai']);
        Permission::create(['name' => 'Mengubah data pegawai lain']);
        Permission::create(['name' => 'Melihat data pegawai terhapus']);
        Permission::create(['name' => 'Mengembalikan data pegawai terhapus']);

        Permission::create(['name' => 'Melihat daftar mssubdirektorat']);
        Permission::create(['name' => 'Menambah data mssubdirektorat']);
        Permission::create(['name' => 'Mengubah data mssubdirektorat']);
        Permission::create(['name' => 'Melihat data mssubdirektorat terhapus']);
        Permission::create(['name' => 'Mengembalikan data mssubdirektorat terhapus']);

        Permission::create(['name' => 'Melihat daftar msseksi']);
        Permission::create(['name' => 'Menambah data msseksi']);
        Permission::create(['name' => 'Mengubah data msseksi']);
        Permission::create(['name' => 'Melihat data msseksi terhapus']);
        Permission::create(['name' => 'Mengembalikan data msseksi terhapus']);

        Permission::create(['name' => 'Melihat daftar msjeniskegiatan']);
        Permission::create(['name' => 'Menambah data msjeniskegiatan']);
        Permission::create(['name' => 'Mengubah data msjeniskegiatan']);
        Permission::create(['name' => 'Melihat data msjeniskegiatan terhapus']);
        Permission::create(['name' => 'Mengembalikan data msjeniskegiatan terhapus']);

        Permission::create(['name' => 'Melihat daftar mspusdakim']);
        Permission::create(['name' => 'Menambah data mspusdakim']);
        Permission::create(['name' => 'Mengubah data mspusdakim']);
        Permission::create(['name' => 'Melihat data mspusdakim terhapus']);
        Permission::create(['name' => 'Mengembalikan data mspusdakim terhapus']);

        Permission::create(['name' => 'Melihat daftar msruangan']);
        Permission::create(['name' => 'Menambah data msruangan']);
        Permission::create(['name' => 'Mengubah data msruangan']);
        Permission::create(['name' => 'Melihat data msruangan terhapus']);
        Permission::create(['name' => 'Mengembalikan data msruangan terhapus']);

        Permission::create(['name' => 'Melihat daftar mscontainment']);
        Permission::create(['name' => 'Menambah data mscontainment']);
        Permission::create(['name' => 'Mengubah data mscontainment']);
        Permission::create(['name' => 'Melihat data mscontainment terhapus']);
        Permission::create(['name' => 'Mengembalikan data mscontainment terhapus']);

        Permission::create(['name' => 'Melihat daftar msrak']);
        Permission::create(['name' => 'Menambah data msrak']);
        Permission::create(['name' => 'Mengubah data msrak']);
        Permission::create(['name' => 'Melihat data msrak terhapus']);
        Permission::create(['name' => 'Mengembalikan data msrak terhapus']);


        // create roles and assign existing permissions
        
        $role = Role::create(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([	'Melihat daftar pegawai', 
        							'Menambah data pegawai', 
        							'Mengubah data pegawai',
                                    'Mengubah data pegawai lain',
                                    'Mengembalikan data pegawai terhapus',
                                    'Melihat daftar mssubdirektorat', 
                                    'Menambah data mssubdirektorat', 
                                    'Mengubah data mssubdirektorat',
                                    'Melihat data mssubdirektorat terhapus',
                                    'Mengembalikan data mssubdirektorat terhapus',
                                    'Melihat daftar msseksi', 
                                    'Menambah data msseksi', 
                                    'Mengubah data msseksi',
                                    'Melihat data msseksi terhapus',
                                    'Mengembalikan data msseksi terhapus',
                                    'Melihat daftar msjeniskegiatan', 
                                    'Menambah data msjeniskegiatan', 
                                    'Mengubah data msjeniskegiatan',
                                    'Melihat data msjeniskegiatan terhapus',
                                    'Mengembalikan data msjeniskegiatan terhapus',
                                    'Melihat daftar mspusdakim', 
                                    'Menambah data mspusdakim', 
                                    'Mengubah data mspusdakim',
                                    'Melihat data mspusdakim terhapus',
                                    'Mengembalikan data mspusdakim terhapus',
                                    'Melihat daftar msruangan', 
                                    'Menambah data msruangan', 
                                    'Mengubah data msruangan',
                                    'Melihat data msruangan terhapus',
                                    'Mengembalikan data msruangan terhapus',
                                    'Melihat daftar mscontainment', 
                                    'Menambah data mscontainment', 
                                    'Mengubah data mscontainment',
                                    'Melihat data mscontainment terhapus',
                                    'Mengembalikan data mscontainment terhapus',
                                    'Melihat daftar msrak', 
                                    'Menambah data msrak', 
                                    'Mengubah data msrak',
                                    'Melihat data msrak terhapus',
                                    'Mengembalikan data msrak terhapus'
        						]);

        $role = Role::create(['name' => 'DIREKTUR']);
        $role->givePermissionTo([   'Melihat daftar pegawai', 
                                    'Mengubah data pegawai'
                                ]);

        $role = Role::create(['name' => 'KASUBDIT']);
        $role->givePermissionTo([   'Melihat daftar pegawai', 
                                    'Mengubah data pegawai'
                                ]);

        $role = Role::create(['name' => 'KASI']);
        $role->givePermissionTo([   'Melihat daftar pegawai', 
                                    'Mengubah data pegawai'
                                ]);

       	$role = Role::create(['name' => 'PEGAWAI']);
        $role->givePermissionTo([	'Melihat daftar pegawai', 
                                    'Mengubah data pegawai'
        						]);

    }
}
