<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\Mssubdirektorat;


class MsSubdirektoratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file = storage_path() . '/app/seeder/ms_subdirektorat.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'SUBDIREKTORAT') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_subdirektorat' => $row[0],
                            'nama_subdirektorat' => $row[1],
                        ];

                        $this->fillData(new MsSubdirektorat(), $data);
                    }
                }

            }

        } 

        $reader->close();

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }
}