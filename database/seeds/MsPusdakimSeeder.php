<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\MsPusdakim;


class MsPusdakimSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file = storage_path() . '/app/seeder/ms_pusdakim.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'PUSDAKIM') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_pusdakim' => $row[0],
                            'nama_pusdakim' => $row[1],
                            'lokasi_pusdakim' => $row[2],
                            'keterangan' => $row[3],
                        ];

                        $this->fillData(new MsPusdakim(), $data);
                    }
                }

            }

        } 

        $reader->close();

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }
}