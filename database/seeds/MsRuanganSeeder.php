<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\MsRuangan;

class MsRuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = storage_path() . '/app/seeder/ms_ruangan.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'RUANGAN') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_ruangan' => $row[0],
                            'nama_ruangan' => $row[1],
                            'keterangan' => $row[2],
                            'kode_pusdakim' => $row[3],
                        ];

                        $this->fillData(new MsRuangan(), $data);
                    }
                }

            }

        } 

        $reader->close();
    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }
}
