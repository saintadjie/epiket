<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\MsSeksi;

class MsSeksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file = storage_path() . '/app/seeder/ms_seksi.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'SEKSI') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'kode_seksi' => $row[0],
                            'nama_seksi' => $row[1],
                            'kode_subdirektorat' => $row[2],
                        ];

                        $this->fillData(new MsSeksi(), $data);
                    }
                }

            }

        } 

        $reader->close();

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }
}
